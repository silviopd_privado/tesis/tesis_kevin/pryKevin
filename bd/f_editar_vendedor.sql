﻿select * from f_editar_vendedor('46705018','peña','diaz','silvio',3,'las viñas 149 - san isidro','978438896','silviopd01@gmail.com',
					'[{"departamentoid":"01","provinciaid":"01"},{"departamentoid":"03","provinciaid":"03"}]','2017-02-28','I')

CREATE OR REPLACE FUNCTION public.f_editar_vendedor(
    IN p_dni character,
    IN p_apellidopaterno character varying,
    IN p_apellidomaterno character varying,
    IN p_nombre character varying,
    IN p_cargo integer,
    IN p_direccion character varying,
    IN p_telefono character varying,
    IN p_correo character varying,
    IN p_detalle_departamentoprovincia json,
    IN p_fecha_baja date,
    IN p_estado character)
  RETURNS TABLE(nv char) AS
$BODY$
	declare
		v_numero_vendedor integer;		
		v_dni char(8);
		v_vendedordetalleid integer;
		
		v_detalle_venta_cursor refcursor;
		v_detalle_venta_registro record;

	begin
		begin

			
			UPDATE public.vendedordetalle
			 SET estado='I',fecha_baja=p_fecha_baja
			 WHERE dni=p_dni;

			UPDATE public.personal
				SET apellidopaterno=p_apellidopaterno, apellidomaterno=p_apellidomaterno, nombre=p_nombre, cargo=p_cargo, 
				direccion=p_direccion, telefono=p_telefono, correo=p_correo
			WHERE dni=p_dni;

			--Inicio: Insertar en venta_detalle
			open v_detalle_venta_cursor for
				select
					departamentoid,
					provinciaid
				from
					json_populate_recordset
					(
						null:: vendedordetalle,
						p_detalle_departamentoprovincia
					);


			loop --Bucle para recorrer todos los registros almacenados en la vaiable: v_detalle_venta_cursor
				fetch v_detalle_venta_cursor into v_detalle_venta_registro; --captura registro por registro
				if found then --Si aun encuentra registros

					if p_estado = 'I' then

						UPDATE public.personal
						   SET estado=p_estado
						 WHERE dni=p_dni;
					
						UPDATE public.vendedordetalle
						 SET estado='I',fecha_baja=p_fecha_baja
						 WHERE dni=p_dni;
					else

						select vendedordetalleid into v_vendedordetalleid  from vendedordetalle where departamentoid = v_detalle_venta_registro.departamentoid and provinciaid = v_detalle_venta_registro.provinciaid and dni = p_dni;

						if v_vendedordetalleid >=0 THEN
						
							UPDATE public.vendedordetalle
							   SET estado='A',fecha_baja = null
							 WHERE vendedordetalleid = v_vendedordetalleid;
							 
						else
							select numero + 1 into v_numero_vendedor 
							from correlativo where tabla = 'vendedordetalle';
							
							INSERT INTO public.vendedordetalle(vendedordetalleid, dni, provinciaid, departamentoid,fecha_alta)
							VALUES (v_numero_vendedor, p_dni, v_detalle_venta_registro.provinciaid, v_detalle_venta_registro.departamentoid,p_fecha_baja);

							update correlativo set numero = v_numero_vendedor where tabla = 'vendedordetalle';											
						end if;
						
					end if;
				else
					exit; --salir del bucle
				end if;
			end loop;
			--Fin: Insertar en venta_detalle

		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
		end;

		--return 1; --Cuando la transacción terminó con exito
		return query select p_dni;
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE