﻿CREATE OR REPLACE FUNCTION public.f_anular(IN p_orden int, IN p_fecha_cancelada date)
  RETURNS TABLE(ordenid integer) AS
$BODY$
DECLARE
		v_credito integer;
		v_mayorista bigint;
		v_saldoorden numeric;
		v_saldocredito numeric;
		v_saldototal numeric;
		v_estado varchar;
	
	begin	

		begin

			select estado into v_estado from ordendecompra WHERE ordendecompra.ordenid = p_orden;

			if v_estado = 'CA' then RAISE EXCEPTION 'El número de orden con código (%) ya se ha cancelado .... ', p_orden; end if;
			
			UPDATE ordendecompra SET estado= 'CA',fechapago=p_fecha_cancelada WHERE ordendecompra.ordenid = p_orden;	

			select numero + 1 into v_credito from correlativo where tabla = 'credito';

			select mayoristaid into v_mayorista from ordendecompra where ordendecompra.ordenid = p_orden;

			select total into v_saldoorden from ordendecompra where ordendecompra.ordenid = p_orden;

			select saldoactual into v_saldocredito from credito where mayoristaid = v_mayorista order by creditoid desc limit 1;
			
			if v_saldocredito IS NULL then v_saldocredito = 0; end if;
			
			INSERT INTO public.credito(creditoid, mayoristaid, saldoactual,saldofavor) VALUES (v_credito, v_mayorista, v_saldoorden + v_saldocredito,v_saldoorden);

			update correlativo set numero = v_credito where tabla = 'credito';

			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_credito;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE

  UPDATE public.ordendecompra SET estado= 'CA',fechapago='2017-01-29' WHERE ordenid = 1

  select * from f_anular(2,'2017-01-29')

  select * from ordendecompra

  select * delete from credito

  select numero + 1 from correlativo where tabla = 'credito';