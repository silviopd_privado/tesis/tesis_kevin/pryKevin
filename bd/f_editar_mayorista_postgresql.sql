﻿select * from mayorista

select * from f_editar_mayorista(20212331377,'GRUPO DELTRON S.A.','ali.castro@deltron.com.pe','951202370','','CAL.RAUL REBAGLIATI NRO. 170 URB. SANTA CATALINA LIMA - LIMA - LA VICTORIA','1','14','A',150)

CREATE OR REPLACE FUNCTION public.f_editar_mayorista(
    IN p_mayoristaid bigint,
    IN p_razonsocial character varying,
    IN p_email character varying,
    IN p_telefono character varying,
    IN p_webservice character varying,
    IN p_direccion character varying,
    IN p_provinciaid character(2),
    IN p_departamentoid character(2),
    IN p_estado character(1),
    IN p_credito numeric)
  RETURNS TABLE(mayorista bigint, credito integer) AS
$BODY$
	DECLARE

		v_credito integer;
	begin	

		begin

			UPDATE public.mayorista
			SET razonsocial=p_razonsocial, email=p_email, telefono=p_telefono, webservice=p_webservice, 
                        direccion=p_direccion,provinciaid=p_provinciaid,departamentoid=p_departamentoid,estado = p_estado
			WHERE mayoristaid=p_mayoristaid;


			select numero + 1 into v_credito from correlativo where tabla = 'credito';

			INSERT INTO public.credito(creditoid, mayoristaid, saldoactual, diasvencidos)
			VALUES (v_credito, p_mayoristaid, p_credito, 45);

			update correlativo set numero = v_credito where tabla = 'credito';
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_mayoristaid,v_credito;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE