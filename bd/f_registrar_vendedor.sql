﻿select * from f_registrar_vendedor('46705014','peña','diaz','silvio',3,'las viñas 149 - san isidro','978438896','silviopd01@gmail.com',
					'[{"departamentoid":"01","provinciaid":"01"},{"departamentoid":"02","provinciaid":"02"}]')

CREATE OR REPLACE FUNCTION public.f_registrar_vendedor(
    IN p_dni character,
    IN p_apellidopaterno varchar,
    IN p_apellidomaterno varchar,
    IN p_nombre varchar,
    IN p_cargo int,
    IN p_direccion varchar,
    IN p_telefono varchar,
    IN p_correo varchar,
    IN p_detalle_departamentoprovincia json)
  RETURNS TABLE(nv integer) AS
$BODY$
	declare
		v_numero_vendedor integer;		
		v_dni char(8);
		
		v_detalle_venta_cursor refcursor;
		v_detalle_venta_registro record;

	begin
		begin

			select dni into v_dni from personal where dni = p_dni;

			if v_dni is not null then
				RAISE EXCEPTION 'El vendedor con dni (%) ya está  registrado...', p_dni;
			end if;
	
			INSERT INTO public.personal(dni, apellidopaterno, apellidomaterno, nombre, cargo, direccion,telefono, correo)
			VALUES (p_dni, p_apellidopaterno, p_apellidomaterno, p_nombre, p_cargo, p_direccion,p_telefono, p_correo);

			--Inicio: Insertar en venta_detalle
			open v_detalle_venta_cursor for
				select
					departamentoid,
					provinciaid
				from
					json_populate_recordset
					(
						null:: vendedordetalle,
						p_detalle_departamentoprovincia
					);


			loop --Bucle para recorrer todos los registros almacenados en la vaiable: v_detalle_venta_cursor
				fetch v_detalle_venta_cursor into v_detalle_venta_registro; --captura registro por registro
				if found then --Si aun encuentra registros

					select numero + 1 into v_numero_vendedor 
					from correlativo where tabla = 'vendedordetalle';
					
					INSERT INTO public.vendedordetalle(vendedordetalleid, dni, provinciaid, departamentoid)
					VALUES (v_numero_vendedor, p_dni, v_detalle_venta_registro.provinciaid, v_detalle_venta_registro.departamentoid);

					update correlativo set numero = v_numero_vendedor where tabla = 'vendedordetalle';
				else
					exit; --salir del bucle
				end if;
			end loop;
			--Fin: Insertar en venta_detalle

		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
		end;

		--return 1; --Cuando la transacción terminó con exito
		return query select v_numero_vendedor;
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE