﻿select * from f_listar_provincia_departamento('lima')

CREATE OR REPLACE FUNCTION public.f_listar_provincia_departamento(IN p_nombre varchar)
  RETURNS TABLE(departamentoprovinciaid character varying, nombre character varying) AS
$BODY$
		
	begin
		return query
		select tb1.departamentoprovinciaid,tb1.nombre from (
			(SELECT (d.departamentoid||''||p.provinciaid)::varchar as departamentoprovinciaid,(d.nombredepartamento||' - '|| p.nombreprovincia)::varchar as nombre,p.nombreprovincia as provincia
			FROM public.departamento d inner join public.provincia p on d.departamentoid=p.departamentoid)
			EXCEPT
			(select (v.departamentoid||''||v.provinciaid)::varchar as departamentoprovinciaid,(d.nombredepartamento||' - '|| p.nombreprovincia)::varchar as nombre,p.nombreprovincia as provincia
			from vendedordetalle v inner join provincia p on p.provinciaid = v.provinciaid and p.departamentoid = v.departamentoid 
			inner join departamento d on d.departamentoid = v.departamentoid)
		) as tb1 
		where lower(tb1.provincia) like	p_nombre																															
		order by 2;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE