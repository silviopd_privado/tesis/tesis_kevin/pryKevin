﻿
select * from f_registrar_data('12345678910',46705014,'987654321',"26-01-2017",'[{"clienteid":"15975312","razonsocial":"54586125","ciudad":"asdasd","factura":"2454211","fecha":26-01-2017,"partnumberproducto":"dfdfsdf","cantidad":10}]')

--clienteid,razonsocial,ciudad,	factura,fecha,partnumberproducto,cantidad,stock



CREATE OR REPLACE FUNCTION public.f_registrar_data(
    IN p_ruc_mayorista varchar,
    IN p_usuario int,
    IN p_fecha date,
    IN p_data json)
  RETURNS varchar AS
$BODY$
	declare
		v_numero_venta integer;
		v_numero_detalleventa integer;
		v_numero_historiaextraccion integer;
		v_fecha date;
		v_cliente varchar;
		v_provinciaid varchar;
	
		v_data_cursor refcursor;
		v_data_registro record;

	begin
		begin
			
		

			--Inicio: generar el nuevo numero de venta
			select numero + 1 into v_numero_venta 
			from correlativo where tabla = 'venta';

			--Inicio: Insertar en venta_detalle
			open v_data_cursor for
				select
					clienteid,
					razonsocial,
					ciudad,
					factura,
					fecha,
					partnumberproducto,
					cantidad
				from
					json_populate_recordset
					(
						null:: data,
						p_data
					);
			
			loop --Bucle para recorrer todos los registros almacenados en la variable: v_detalle_venta_cursor
				fetch v_data_cursor into v_data_registro; --captura registro por registro
				if found then --Si aun encuentra registros

					--validar que no se repita la extracción de la data por fecha
					select fecha into v_fecha from venta where fecha = p_fecha;
					
					--if v_fecha IS NULL OR v_fecha = '' then		
							
						select clienteid into v_cliente from cliente where clienteid like v_data_registro.clienteid;

						--comprobando que el clienteid no sea vacio
						if v_cliente IS NULL OR v_cliente = '' then

							SELECT provincia.provinciaid into v_provinciaid FROM public.provincia,public.departamento WHERE departamento.departamentoid = provincia.departamentoid AND provincia.nombreprovincia like v_data_registro.ciudad ;
							
							INSERT INTO public.cliente(clienteid, razonsocial, provinciaid) VALUES (v_cliente, v_data_registro.razonsocial, v_provinciaid);
						end if;
						

						--inserción a la tabla venta
						INSERT INTO public.venta(ventaid, fecha, factura, clienteid, mayoristaid)
						VALUES (v_numero_venta, v_data_registro.fecha, v_data_registro.factura, v_cliente, p_ruc_mayorista);


						--generar el nuevo numero de detalleventa
						select numero + 1 into v_numero_detalleventa 
						from correlativo where tabla = 'detalleventa';

						--inserción a la tabla detalleventa					
						INSERT INTO public.detalleventa(detalleventaid, productoid, clienteid, ventaid, cantidad, usuarioid)
						VALUES (v_numero_detalleventa, v_data_registro.partnumberproducto, v_cliente, v_numero_venta, v_cliente.cantidad, p_usuario);
					--end if;

				else
					exit; --salir del bucle
				end if;
			end loop;
			--Fin: 

			--generar el nuevo numero de detalleventa
					select numero + 1 into v_numero_historiaextraccion 
					from correlativo where tabla = 'historiaextraccion';

			--inserción tabla fecha
			INSERT INTO public.historiaextraccion(historiaid, mayoristaid)
			VALUES (v_numero_historiaextraccion, p_ruc_mayorista);


		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
		end;

		--return 1; --Cuando la transacción terminó con exito
		return 'Ingreso de data satisfactoriamente...';
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE

  --RUC_MAYORITA,RUC_CLIENTE,RAZON_SOCIAL_CLIENTE,CIUDAD,FACTURA,FECHA,CODIGO_PRODUCTO,CANTIDAD