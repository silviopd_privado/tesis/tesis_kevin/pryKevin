﻿select * from f_leerdatos_mayorista(20127745910)

CREATE OR REPLACE FUNCTION public.f_leerdatos_mayorista(IN p_mayorista bigint)
  RETURNS TABLE(ruc bigint, razonsocial character varying, email character varying, telefono character varying, direccion character varying, webservice character varying, estado char(1), provincia char(2), departamento char(2), saldoactual numeric) AS
$BODY$
		
	begin
		return query
		SELECT 
                    distinct mayorista.mayoristaid as rc, 
                    mayorista.razonsocial as rs, 
                    mayorista.email as em, 
                    mayorista.telefono as em, 
                    mayorista.direccion as di, 
                    mayorista.webservice as ws, 
                    mayorista.estado as est, 
                    mayorista.provinciaid as pro,
                    mayorista.departamentoid as dep,
                    (select credito.saldoactual from public.credito where credito.mayoristaid = mayorista.mayoristaid order by 1 desc limit 1) as sa
                  FROM 
                    public.mayorista,
                    public.credito
                  WHERE                     
                    credito.mayoristaid = mayorista.mayoristaid AND mayorista.mayoristaid=p_mayorista ;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE

  char(2)