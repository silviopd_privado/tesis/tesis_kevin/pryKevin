﻿CREATE OR REPLACE FUNCTION public.f_listar_tipocategoria(IN p_codigo_categoria integer)
  RETURNS TABLE(codigo_tipoCategoria integer, categoria character varying, tipocategoria character varying, comentario character varying) AS
$BODY$
begin
	return query
	
	SELECT tc.tipocategoriaid, 
	c.descripcion as cate,
	tc.descripcion as tipocat, 
	tc.comentario as comen
	FROM public.categoria c inner join public.tipocategoria tc
	on c.categoriaid = tc.categoriaid
	where
	(case p_codigo_categoria
	when 0 then
	1=1
	else
	c.categoriaid = p_codigo_categoria
	end)
	order by
	tc.tipocategoriaid;
end
$BODY$
  LANGUAGE plpgsql VOLATILE

  select * from f_listar_tipocategoria(0)