﻿CREATE OR REPLACE FUNCTION public.f_listar_producto(IN p_codigo_tipocategoria integer)
  RETURNS TABLE(codigo integer,codigoproducto varchar, modelo character varying, descripcion character varying, capacidad character varying, categoria character varying, tipocategoria character varying,precio_venta numeric(14,2), estado character varying) AS
$BODY$
		
	begin
		return query
		SELECT 
		  producto.productoid,
		  producto.codigoproducto as codpro, 
		  producto.modelo as mod, 
		  producto.descripcion as descri, 
		  producto.capacidad as cap,  
		  categoria.descripcion as cat,
		  tipocategoria.descripcion as tipocate, 
		  producto.precio_venta as pre,
		  (case producto.estado when 'A' then 'ACTIVO' else 'INACTIVO' end)::varchar as est
		FROM 
		  public.producto, 
		  public.categoria, 
		  public.tipocategoria
		WHERE 
		  producto.tipocategoriaid = tipocategoria.tipocategoriaid AND
		  categoria.categoriaid = tipocategoria.categoriaid
		  AND	
			(case p_codigo_tipocategoria
				when 0 then 
					1=1
				else
					tipocategoria.tipocategoriaid = p_codigo_tipocategoria
			end)
			
			
		order by
			producto.modelo;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE

  select * from f_listar_producto(1)