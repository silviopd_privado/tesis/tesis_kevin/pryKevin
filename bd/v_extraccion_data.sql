﻿	drop view v_listar

  create or replace view v_listar as 
  select a.nombre as partnumber,c.codigo_cliente as ruc_cliente,(c.apellido_paterno || ' ' ||  c.apellido_materno || ' ' ||  c.nombres)::varchar as razonsocial,
	 p.nombre as provincia ,v.numero_documento as factura,v.fecha_venta,vd.cantidad,a.stock
  from articulo a left join venta_detalle vd on vd.codigo_articulo = a.codigo_articulo 
	left join venta v on vd.numero_venta=v.numero_venta
	left join cliente c on c.codigo_cliente = v.codigo_cliente
	left join provincia p on c.codigo_provincia = p.codigo_provincia and c.codigo_departamento = p.codigo_departamento;



select * from v_listar

select * from venta