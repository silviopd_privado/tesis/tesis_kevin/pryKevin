<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["dni"]) || !isset($_POST["apellidopaterno"]) || !isset($_POST["apellidomaterno"]) || !isset($_POST["nombre"]) || !isset($_POST["cargo"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$dni = $_POST["dni"];
$apellidopaterno = $_POST["apellidopaterno"];
$apellidomaterno = $_POST["apellidomaterno"];
$nombre = $_POST["nombre"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];
$correo = $_POST["correo"];
$cargo = $_POST["cargo"];

$fecha_alta = date('Y-m-d');

$departamentoprovincia =  $_POST["jsonDetalle"];

try {
    if (validarToken($token)) {

        $obj = new Personal();
        $obj->setDni($dni);
        $obj->setApellidopaterno($apellidopaterno);
        $obj->setApellidomaterno($apellidomaterno);
        $obj->setNombre($nombre);
        $obj->setCargo($cargo);
        $obj->setDireccion($direccion);
        $obj->setTelefono($telefono);
        $obj->setCorreo($correo);
        $obj->setDepartamentoprovincia($departamentoprovincia);
        $obj->setFecha_alta($fecha_alta);        

        $resultado = $obj->agregarvendedor();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
