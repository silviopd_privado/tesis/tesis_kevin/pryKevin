<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Mayorista.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

//if (!isset($_POST["token"])) {
//    Funciones::imprimeJSON(500, "Debe especificar un token", "");
//    exit();
//}

//$token = $_POST["token"];

try {
//    if (validarToken($token)) {
        $obj = new Mayorista();

        $valorBusqueda = $_GET["term"];

        $resultado = $obj->cargarDatosMayorista($valorBusqueda);

//echo '<pre>';
//print_r($resultado);
//echo '</pre>';

        $datos = array();

        for ($i = 0; $i < count($resultado); $i++) {
            $registro = array(
                'label' => $resultado[$i]["razonsocial"],
                'value' => array(
                    'mayoristaid' => $resultado[$i]["mayoristaid"],
                    'razonsocial' => $resultado[$i]["razonsocial"],
                    'direccion' => $resultado[$i]["direccion"],
                    'telefono' => $resultado[$i]["telefono"],
                    'saldoactual' => $resultado[$i]["saldoactual"]
                )
            );
            $datos[$i] = $registro;
        }

        echo json_encode($datos);
//    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}