<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Mayorista.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["rucmayorista"])) {
    Funciones::imprimeJSON(500, "Debe especificar un mayorista", "");
    exit();
}

$token = $_POST["token"];
$usuario = $_POST["rucmayorista"];

try {
    if (validarToken($token)) {
        $obj = new Mayorista();
        $resultado = $obj->mayoristaWS($usuario);

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "mayoristaws" => $resultado[$i]["webservice"]
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}