<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Categoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["categoriaid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$tipocategoriaid = $_POST["categoriaid"];

try {
    if (validarToken($token)) {
        $obj = new Categoria();
        $resultado = $obj->leerDatos($tipocategoriaid);

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "categoriaID" => $resultado[$i]["categoriaid"],
                "descripcion" => $resultado[$i]["descripcion"],
                "observacion" => $resultado[$i]["observacion"]
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}