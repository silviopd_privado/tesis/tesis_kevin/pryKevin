<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/TipoCategoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["categoriaid"]) ||!isset($_POST["descripcion"]) || !isset($_POST["comentario"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$p_categoriaid = $_POST["categoriaid"];
$p_descripcion = $_POST["descripcion"];
$p_comentario = $_POST["comentario"];

try {
    if (validarToken($token)) {

        $obj = new TipoCategoria();
        $obj->setCategoriaID($p_categoriaid);
        $obj->setDescripcion($p_descripcion);
        $obj->setComentario($p_comentario);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}