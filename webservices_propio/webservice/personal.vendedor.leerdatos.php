<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["dni"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$dni = $_POST["dni"];

try {
    if (validarToken($token)) {
        $obj = new Personal();
        $resultado = $obj->leerDatosvendedor($dni);

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "dni" => $resultado[$i]["dni"],
                "apellidopaterno" => $resultado[$i]["apellidopaterno"],
                "apellidomaterno" => $resultado[$i]["apellidomaterno"],
                "nombre" => $resultado[$i]["nombre"],
                "cargo" => $resultado[$i]["cargo"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "correo" => $resultado[$i]["correo"],
                "estado" => $resultado[$i]["estado"],
                "departamentoprovinciaid" => $resultado[$i]["departamentoprovinciaid"],
                "nombredepartamentoprovincia" => $resultado[$i]["nombredepartamentoprovincia"]               
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}