<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["productoid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$productoid = $_POST["productoid"];

try {
    if (validarToken($token)) {
        $obj = new Producto();
        $resultado = $obj->eliminar($productoid);

        Funciones::imprimeJSON(200, "Se Elimino Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}