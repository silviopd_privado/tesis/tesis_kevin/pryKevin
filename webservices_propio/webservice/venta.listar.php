<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Venta.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (! isset($_POST["token"])){
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        
        $fechaInicio = $_POST["fechaInicio"];
        $fechaFinal = $_POST["fechaFinal"];
        $tipo = $_POST["tipo"];
        
        $obj = new Venta();        
        
        $resultado = $obj->listar($fechaInicio, $fechaFinal, $tipo);        
        
        Funciones::imprimeJSON(200, "", $resultado);
        }
} catch (Exception $exc) {
    
    $mensajeError = $exc->getMessage();
    $posicion = strpos($mensajeError, "Raise exception:");
    if ($posicion>0) {
        $mensajeError = substr($mensajeError, $posicion+27,  strlen($mensajeError));
    }
    
    Funciones::imprimeJSON(500, $mensajeError, "");
}