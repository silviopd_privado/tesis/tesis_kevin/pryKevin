<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Provincia.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["departamentoid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$departamentoid = $_POST["departamentoid"];

try {
    if (validarToken($token)) {
        $obj = new Provincia();
        $resultado = $obj->listar($departamentoid);

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "provinciaid" => $resultado[$i]["provinciaid"],
                "nombreprovincia" => $resultado[$i]["nombreprovincia"],
                "departamentoid" => $resultado[$i]["departamentoid"]
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}