<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Mayorista.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["mayoristaid"]) || !isset($_POST["razonsocial"]) || !isset($_POST["email"]) || !isset($_POST["telefono"]) || !isset($_POST["direccion"]) || !isset($_POST["provinciaid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$mayoristaid = $_POST["mayoristaid"];
$razonsocial = $_POST["razonsocial"];
$email = $_POST["email"];
$telefono = $_POST["telefono"];
$webservice = $_POST["webservice"];
$direccion = $_POST["direccion"];
$departamentoid = $_POST["departamentoid"];
$provinciaid = $_POST["provinciaid"];
$credito = $_POST["credito"];

try {
    if (validarToken($token)) {
        $obj = new Mayorista();
        $obj->setMayoristaid($mayoristaid);
        $obj->setRazonsocial($razonsocial);
        $obj->setEmail($email);
        $obj->setTelefono($telefono);
        $obj->setWebservice($webservice);
        $obj->setDireccio($direccion);
        $obj->setProvinciaid($provinciaid);
        $obj->setDepartamentoid($departamentoid);
        $obj->setCredito($credito);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
