<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Categoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["categoriaid"]) || !isset($_POST["descripcion"]) || !isset($_POST["observacion"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$categoriaID = $_POST["categoriaid"];
$p_descripcion = $_POST["descripcion"];
$p_observacion = $_POST["observacion"];

try {
    if (validarToken($token)) {

        $obj = new Categoria();
        $obj->setCategoriaID($categoriaID);
        $obj->setDescripcion($p_descripcion);
        $obj->setObservacion($p_observacion);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}