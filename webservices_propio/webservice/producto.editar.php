<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["productoid"]) || !isset($_POST["modelo"]) || !isset($_POST["descripcion"])|| !isset($_POST["capacidad"])|| !isset($_POST["estado"])|| !isset($_POST["tipocategoriaid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$productoid = $_POST["productoid"];
$codigoproducto = $_POST["codigoproducto"];
$modelo = $_POST["modelo"];
$descripcion = $_POST["descripcion"];
$capacidad = $_POST["capacidad"];
$estado = $_POST["estado"];
$tipocategoriaid = $_POST["tipocategoriaid"];

try {
    if (validarToken($token)) {

        $obj = new Producto();
        $obj->setProductoid($productoid);
        $obj->setCodigoproducto($codigoproducto);
        $obj->setModelo($modelo);
        $obj->setDescripcion($descripcion);
        $obj->setCapacidad($capacidad);
        $obj->setEstado($estado);
        $obj->setTipocategoriaid($tipocategoriaid);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}