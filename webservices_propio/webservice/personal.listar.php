<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Personal();
        $resultado = $obj->listar();

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "dni" => $resultado[$i]["dni"],
                "personal" => $resultado[$i]["personal"],
                "cargo" => $resultado[$i]["cargo"],
                "cargoid" => $resultado[$i]["cargoid"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "correo" => $resultado[$i]["correo"],
                "estado" => $resultado[$i]["estado"],
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}