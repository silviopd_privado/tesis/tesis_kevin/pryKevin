<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Vendedor.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

//if (!isset($_POST["token"])) {
//    Funciones::imprimeJSON(500, "Debe especificar un token", "");
//    exit();
//}

//$token = $_POST["token"];

try {
//    if (validarToken($token)) {
        $obj = new Vendedor();

        $valorBusqueda = $_GET["term"];

        $resultado = $obj->cargarDatosVendedorDepartamentoProvincia($valorBusqueda);

//echo '<pre>';
//print_r($resultado);
//echo '</pre>';

        $datos = array();

        for ($i = 0; $i < count($resultado); $i++) {
            $registro = array(
                'label' => $resultado[$i]["nombre"],
                'value' => array(
                    'departamentoprovinciaid' => $resultado[$i]["departamentoprovinciaid"],
                    'nombre' => $resultado[$i]["nombre"]
                )
            );
            $datos[$i] = $registro;
        }

        echo json_encode($datos);
//    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}