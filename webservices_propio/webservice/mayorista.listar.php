<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Mayorista.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Mayorista();
        $resultado = $obj->listar();

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "ruc" => $resultado[$i]["ruc"],
                "razonsocial" => $resultado[$i]["razonsocial"],
                "email" => $resultado[$i]["email"],
                "telefono" => $resultado[$i]["telefono"],
                "direccion" => $resultado[$i]["direccion"],
                "provincia" => $resultado[$i]["provincia"],
                "departamento" => $resultado[$i]["departamento"],
                "webservice" => $resultado[$i]["webservice"],
                "estado" => $resultado[$i]["estado"],
                "saldoactual" => $resultado[$i]["saldoactual"],
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}