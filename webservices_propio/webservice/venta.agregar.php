<?php

require_once '../negocio/Venta.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (! isset($_POST["token"])){
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        
        $mayoristaid = $_POST["p_mayoristaid"];
        $pagoid = $_POST["p_pagoid"];
        $codigo_usuario = $_POST["p_codigo_usuario"];
        $fechaVenta = date('Y-m-d');

        $detalle_venta = $_POST["p_detalle_venta"]; //JSON
        
        $obj = new Venta();
        $obj->setMayoristaid($mayoristaid);
        $obj->setPagoid($pagoid);        
        $obj->setCodigo_usuario($codigo_usuario);        
        $obj->setFecha_emitida($fechaVenta);        
        $obj->setDetalle_venta($detalle_venta);
        
        $resultado = $obj->agregar();        
        
        Funciones::imprimeJSON(200, "", $resultado);
        }
} catch (Exception $exc) {
    
    $mensajeError = $exc->getMessage();
    $posicion = strpos($mensajeError, "Raise exception:");
    if ($posicion>0) {
        $mensajeError = substr($mensajeError, $posicion+27,  strlen($mensajeError));
    }
    
    Funciones::imprimeJSON(500, $mensajeError, "");
}