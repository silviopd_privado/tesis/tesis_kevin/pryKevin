<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Configuracion.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $objConf = new Configuracion();

        $codigoParametro = $_POST["p_codigo_parametro"];
        $valorObtenido = $objConf->obtenerValorConfiguracion($codigoParametro);
        Funciones::imprimeJSON(200, "", $valorObtenido);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}