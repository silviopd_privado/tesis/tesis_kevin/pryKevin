<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["productoid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$productoid = $_POST["productoid"];

try {
    if (validarToken($token)) {
        $obj = new Producto();
        $resultado = $obj->leerDatos($productoid);

        $listavendedores = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "codigo" => $resultado[$i]["productoid"],
                "codigoproducto" => $resultado[$i]["codigoproducto"],
                "modelo" => $resultado[$i]["modelo"],
                "descripcion" => $resultado[$i]["descripcion"],
                "capacidad" => $resultado[$i]["capacidad"],
                "categoria" => $resultado[$i]["categoriaid"],
                "tipocategoria" => $resultado[$i]["tipocategoriaid"],
                "estado" => $resultado[$i]["estado"]
            );

            $listavendedores[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listavendedores);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}