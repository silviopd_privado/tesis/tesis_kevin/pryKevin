<?php

require_once '../negocio/Venta.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (! isset($_POST["token"])){
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        
        $numeroVenta = $_POST["p_numeroVenta"];
        $numerocheque= $_POST["p_numerocheque"];
        $fechaVenta = date('Y-m-d');
        
        $obj = new Venta();    
        $obj->setFechapago($fechaVenta);
        $obj->setNumerocheque($numerocheque);
        $resultado = $obj->cancelar($numeroVenta);        
        
        Funciones::imprimeJSON(200, "", $resultado);
        }
} catch (Exception $exc) {
    
    $mensajeError = $exc->getMessage();
    $posicion = strpos($mensajeError, "Raise exception:");
    if ($posicion>0) {
        $mensajeError = substr($mensajeError, $posicion+27,  strlen($mensajeError));
    }
    
    Funciones::imprimeJSON(500, $mensajeError, "");
}