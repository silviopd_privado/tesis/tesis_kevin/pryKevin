<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Venta.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["numeroVenta"])) {
    Funciones::imprimeJSON(500, "Faltan parametros", "");
    exit;
}

$numeroVenta = $_POST["numeroVenta"];

$token = $_POST["token"];

try {

    if (validarToken($token)) {

        $objVenta = new Venta();
        $resultado = $objVenta->listarVentaDetalle($numeroVenta);

        Funciones::imprimeJSON(200, "", $resultado);
    }
} catch (Exception $exc) {
//Funciones::mensaje($exc->getMessage(), "e");
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}

