<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["codigoproducto"]) || !isset($_POST["modelo"]) || !isset($_POST["descripcion"])|| !isset($_POST["capacidad"])|| !isset($_POST["tipocategoriaid"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$codigoproducto = $_POST["codigoproducto"];
$modelo = $_POST["modelo"];
$descripcion = $_POST["descripcion"];
$capacidad = $_POST["capacidad"];
$tipocategoriaid = $_POST["tipocategoriaid"];

try {
    if (validarToken($token)) {

        $obj = new Producto();
        $obj->setCodigoproducto($codigoproducto);
        $obj->setModelo($modelo);
        $obj->setDescripcion($descripcion);
        $obj->setCapacidad($capacidad);
        $obj->setTipocategoriaid($tipocategoriaid);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}