<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

try {
//    if (validarToken($token)) {
    $obj = new Producto();

    $valorBusqueda = $_GET["term"];

    $resultado = $obj->cargarDatosArticulo($valorBusqueda);

//echo '<pre>';
//print_r($resultado);
//echo '</pre>';

    $datos = array();

    for ($i = 0; $i < count($resultado); $i++) {
        $registro = array(
            'label' => $resultado[$i]["codigoproducto"],
            'value' => array(
                'codigo' => $resultado[$i]["productoid"],
                'nombre' => $resultado[$i]["codigoproducto"]
            )
        );
        $datos[$i] = $registro;
    }

    echo json_encode($datos);
//    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}


