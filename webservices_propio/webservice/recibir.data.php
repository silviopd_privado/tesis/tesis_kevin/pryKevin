<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Data.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

        $data = $_POST["data"];

        $obj = new Data();
        $obj->setData($data);

        $resultado = $obj->agregar();
        
} catch (Exception $exc) {

    $mensajeError = $exc->getMessage();
    $posicion = strpos($mensajeError, "Raise exception:");
    if ($posicion > 0) {
        $mensajeError = substr($mensajeError, $posicion + 27, strlen($mensajeError));
    }

    Funciones::imprimeJSON(500, $mensajeError, "");
}

//[{"codigo_articulo":24,"cantidad":1,"precio":60,"descuento1":0,"descuento2":0}]