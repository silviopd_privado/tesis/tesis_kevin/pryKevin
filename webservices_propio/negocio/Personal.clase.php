<?php

require_once '../datos/Conexion.clase.php';

class Personal extends Conexion {

    private $dni;
    private $apellidopaterno;
    private $apellidomaterno;
    private $nombre;
    private $direccion;
    private $telefono;
    private $correo;
    private $cargo;
    private $estado;
    private $departamentoprovincia;
    private $fecha_alta;
    private $fecha_baja;

    function getFecha_alta() {
        return $this->fecha_alta;
    }

    function getFecha_baja() {
        return $this->fecha_baja;
    }

    function setFecha_alta($fecha_alta) {
        $this->fecha_alta = $fecha_alta;
    }

    function setFecha_baja($fecha_baja) {
        $this->fecha_baja = $fecha_baja;
    }

        function getDni() {
        return $this->dni;
    }

    function getApellidopaterno() {
        return $this->apellidopaterno;
    }

    function getApellidomaterno() {
        return $this->apellidomaterno;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getCargo() {
        return $this->cargo;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setApellidopaterno($apellidopaterno) {
        $this->apellidopaterno = $apellidopaterno;
    }

    function setApellidomaterno($apellidomaterno) {
        $this->apellidomaterno = $apellidomaterno;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCorreo() {
        return $this->correo;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function getEstado() {
        return $this->estado;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function getDepartamentoprovincia() {
        return $this->departamentoprovincia;
    }

    function setDepartamentoprovincia($departamentoprovincia) {
        $this->departamentoprovincia = $departamentoprovincia;
    }
    
    public function listar() {
        try {
            $sql = "select p.dni,initcap(p.apellidopaterno || ' ' || p.apellidomaterno || ' ' || p.nombre)::varchar as personal,
                           c.nombre as cargo,p.direccion,p.telefono,p.correo,(case p.estado when 'A' then 'ACTIVO' else 'INACTIVO' end) as estado,
                           c.cargoid
                    from personal p inner join cargo c on p.cargo = c.cargoid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE public.personal
                    SET  apellidopaterno=:p_apellidopaterno, apellidomaterno=:p_apellidomaterno,
                    nombre=:p_nombre, cargo=:p_cargo,
                    direccion=:p_direccion, telefono=:p_telefono, correo=:p_correo, estado=:p_estado
                    WHERE dni=:p_dni";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_cargo", $this->getCargo());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_correo", $this->getCorreo());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla personal.");
        }
    }
    
    public function editarvendedor() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_editar_vendedor(:p_dni,:p_apellidopaterno,:p_apellidomaterno,:p_nombre,:p_cargo,:p_direccion,:p_telefono,:p_correo,:p_departamentoprovincia,:p_fecha_baja,:p_estado)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_cargo", $this->getCargo());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_correo", $this->getCorreo());
            $sentencia->bindParam(":p_departamentoprovincia", $this->getDepartamentoprovincia());            
            $sentencia->bindParam(":p_fecha_baja", $this->getFecha_baja());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla personal.");
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_editar_vendedor(:p_dni,:p_apellidopaterno,:p_apellidomaterno,:p_nombre,:p_cargo,:p_direccion,:p_telefono,:p_correo,:p_departamentoprovincia,:p_fecha_baja)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_cargo", $this->getCargo());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_correo", $this->getCorreo());
            $sentencia->bindParam(":p_departamentoprovincia", $this->getDepartamentoprovincia());            
            $sentencia->bindParam(":p_fecha_baja", $this->getFecha_baja());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function agregarvendedor() {
        $this->dblink->beginTransaction();
        try {
            
            $sql = "select * from f_registrar_vendedor(:p_dni,:p_apellidopaterno,:p_apellidomaterno,:p_nombre,:p_cargo,:p_direccion,:p_telefono,:p_correo,:p_departamentoprovincia,:p_fecha_alta)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_cargo", $this->getCargo());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_correo", $this->getCorreo());
            $sentencia->bindParam(":p_departamentoprovincia", $this->getDepartamentoprovincia());            
            $sentencia->bindParam(":p_fecha_alta", $this->getFecha_alta());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function leerDatos($p_dni) {
        try {
            $sql = "select * from personal where dni = :p_dni;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $p_dni);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function leerDatosvendedor($p_dni) {
        try {
            $sql = "SELECT 
                        personal.dni, 
                        personal.apellidopaterno, 
                        personal.apellidomaterno, 
                        personal.nombre, 
                        personal.cargo, 
                        personal.direccion, 
                        personal.telefono, 
                        personal.correo, 
                        personal.estado, 
                        departamento.departamentoid||''||provincia.provinciaid as departamentoprovinciaid, 
                        departamento.nombredepartamento||' - '||provincia.nombreprovincia as nombredepartamentoprovincia
                      FROM 
                        public.personal, 
                        public.vendedordetalle, 
                        public.departamento, 
                        public.provincia
                      WHERE 
                        personal.dni = vendedordetalle.dni AND
                        vendedordetalle.provinciaid = provincia.provinciaid AND
                        vendedordetalle.departamentoid = provincia.departamentoid AND
                        provincia.departamentoid = departamento.departamentoid AND vendedordetalle.estado = 'A' 
                        AND personal.dni = :p_dni;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $p_dni);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
