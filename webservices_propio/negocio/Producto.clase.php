<?php

require_once '../datos/Conexion.clase.php';

class Producto extends Conexion {

    private $productoid;
    private $codigoproducto;
    private $modelo;
    private $descripcion;
    private $capacidad;
    private $estado;
    private $tipocategoriaid;
    private $stock;

    function getProductoid() {
        return $this->productoid;
    }

    function getModelo() {
        return $this->modelo;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getCapacidad() {
        return $this->capacidad;
    }

    function getEstado() {
        return $this->estado;
    }

    function getTipocategoriaid() {
        return $this->tipocategoriaid;
    }

    function setProductoid($productoid) {
        $this->productoid = $productoid;
    }

    function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setCapacidad($capacidad) {
        $this->capacidad = $capacidad;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setTipocategoriaid($tipocategoriaid) {
        $this->tipocategoriaid = $tipocategoriaid;
    }

    function getCodigoproducto() {
        return $this->codigoproducto;
    }

    function setCodigoproducto($codigoproducto) {
        $this->codigoproducto = $codigoproducto;
    }

    function getPrecio_venta() {
        return $this->precio_venta;
    }

    function setPrecio_venta($precio_venta) {
        $this->precio_venta = $precio_venta;
    }

    function getStock() {
        return $this->stock;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    public function listar($p_tipocategoria) {
        try {
            $sql = "select * from f_listar_producto(:p_tipocategoria)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_tipocategoria", $p_tipocategoria);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_productoid) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from producto where productoid = :p_productoid";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_productoid", $p_productoid);
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {

            //Actualizar el correlativo en +1
            $sql = "select * from f_agregar_producto(:p_codigoproducto, :p_modelo, :p_descripcion, :p_capacidad, :p_tipocategoriaid)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoproducto", $this->getCodigoproducto());
            $sentencia->bindParam(":p_modelo", $this->getModelo());
            $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
            $sentencia->bindParam(":p_capacidad", $this->getCapacidad());
            $sentencia->bindParam(":p_tipocategoriaid", $this->getTipocategoriaid());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function leerDatos($p_productoid) {
        try {
            $sql = "SELECT 
                    producto.productoid, 
                    producto.codigoproducto, 
                    producto.modelo, 
                    producto.descripcion, 
                    producto.capacidad, 
                    categoria.categoriaid, 
                    tipocategoria.tipocategoriaid,
                    producto.estado
                  FROM 
                    public.producto, 
                    public.categoria, 
                    public.tipocategoria
                  WHERE 
                    producto.tipocategoriaid = tipocategoria.tipocategoriaid AND
                    categoria.categoriaid = tipocategoria.categoriaid 
                    AND producto.productoid = :p_productoid;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_productoid", $p_productoid);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();

        try {
            $sql = "UPDATE public.producto
                    SET codigoproducto=:p_codigoproducto, modelo=:p_modelo, descripcion=:p_descripcion, 
                        capacidad=:p_capacidad, estado=:p_estado, 
                        tipocategoriaid=:p_tipocategoriaid
                    WHERE productoid=:p_productoid
               ";

            //Preparar la sentencia
            $sentencia = $this->dblink->prepare($sql);

            //Asignar un valor a cada parametro
            $sentencia->bindParam(":p_codigoproducto", $this->getCodigoproducto());
            $sentencia->bindParam(":p_modelo", $this->getModelo());
            $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
            $sentencia->bindParam(":p_capacidad", $this->getCapacidad());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_tipocategoriaid", $this->getTipocategoriaid());
            $sentencia->bindParam(":p_productoid", $this->getProductoid());

            //Ejecutar la sentencia preparada
            $sentencia->execute();


            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    public function cargarDatosArticulo($p_nombreArticulo) {
        try {
            $sql = "select  
                       productoid,
                        codigoproducto
                    from 
                        producto 
                    where 
                        lower(codigoproducto) like :p_nombreArticulo";

            $sentencia = $this->dblink->prepare($sql);
            $valorBusqueda = '%' . strtolower($p_nombreArticulo) . '%';
            $sentencia->bindParam(":p_nombreArticulo", $valorBusqueda);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
