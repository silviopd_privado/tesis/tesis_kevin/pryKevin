<?php

require_once '../datos/Conexion.clase.php';

class Cargo extends Conexion {
    

    public function listar(){
        try {
            $sql = "select * from cargo order by nombre";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
