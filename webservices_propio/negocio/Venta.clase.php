<?php

require_once '../datos/Conexion.clase.php';

class Venta extends Conexion {

    private $mayoristaid;
    private $pagoid;
    private $fecha_emitida;
    private $codigo_usuario;
    private $detalle_venta;
    private $numerocheque;
    
    private $fechapago;
    
    function getMayoristaid() {
        return $this->mayoristaid;
    }

    function getPagoid() {
        return $this->pagoid;
    }

    function getFecha_emitida() {
        return $this->fecha_emitida;
    }

    function getCodigo_usuario() {
        return $this->codigo_usuario;
    }

    function getDetalle_venta() {
        return $this->detalle_venta;
    }

    function setMayoristaid($mayoristaid) {
        $this->mayoristaid = $mayoristaid;
    }

    function setPagoid($pagoid) {
        $this->pagoid = $pagoid;
    }

    function setFecha_emitida($fecha_emitida) {
        $this->fecha_emitida = $fecha_emitida;
    }

    function setCodigo_usuario($codigo_usuario) {
        $this->codigo_usuario = $codigo_usuario;
    }

    function setDetalle_venta($detalle_venta) {
        $this->detalle_venta = $detalle_venta;
    }

    function getFechapago() {
        return $this->fechapago;
    }

    function setFechapago($fechapago) {
        $this->fechapago = $fechapago;
    }

    function getNumerocheque() {
        return $this->numerocheque;
    }

    function setNumerocheque($numerocheque) {
        $this->numerocheque = $numerocheque;
    }

        
     public function agregar() {
        try {
            $sql = "select * from f_registrar_venta(:p_mayoristaid, :p_pagoid, :p_fecha_emitida, :p_codigo_usuario,:p_detalle_json) as fv";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $this->getMayoristaid());
            $sentencia->bindParam(":p_pagoid", $this->getPagoid());
            $sentencia->bindParam(":p_fecha_emitida", $this->getFecha_emitida());
            $sentencia->bindParam(":p_codigo_usuario", $this->getCodigo_usuario());
            $sentencia->bindParam(":p_detalle_json", $this->getDetalle_venta());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            //fetch es solo para 1 registro
            //fetchALL para mucho registros
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listar($p_fecha1, $p_fecha2, $p_tipo) {
        try {
            $sql = "select * from f_listar_venta(:p_fecha1, :p_fecha2, :p_tipo)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_fecha1", $p_fecha1);
            $sentencia->bindParam(":p_fecha2", $p_fecha2);
            $sentencia->bindParam(":p_tipo", $p_tipo);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    ////
    
    public function cancelar($numeroVenta) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE public.ordendecompra
                    SET fechapago=:p_fechapago, estado='PA',numerocheque=:p_numerocheque
                    WHERE ordenid = :p_numero_venta";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_fechapago", $this->getFechapago());
            $sentencia->bindParam(":p_numero_venta", $numeroVenta);
            $sentencia->bindParam(":p_numerocheque", $this->getNumerocheque());
            $sentencia->execute();
            
            $this->dblink->commit();
            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }
    }
    
     public function anular($numeroVenta) {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_anular(:p_ordenid,:p_fecha)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_ordenid", $numeroVenta);
            $sentencia->bindParam(":p_fecha", $this->getFechapago());
            $sentencia->execute();

            //Terminar la transacción
            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }
    }

    public function listarVentaDetalle($numeroVenta) {
        try {
            $sql = "SELECT 
                    ordendecompra.ordenid, 
                    ordendecompra.fechaemitida, 
                    ordendecompra.fechapago, 
                    (case ordendecompra.estado when 'EM' then 'EMITIDO' when 'PA' then 'PAGADO' else 'CANCELADO' end)::varchar as estado, 
                    upper(formaspago.tipopago)::varchar as tipopago, 
                    ordendecompra.porcentaje_igv, 
                    ordendecompra.sub_total, 
                    ordendecompra.igv, 
                    (ordendecompra.sub_total + ordendecompra.igv)::numeric as total,
                    mayorista.razonsocial, 
                    detalleordendecompra.detalleid, 
                    producto.codigoproducto, 
                    detalleordendecompra.cantidad, 
                    detalleordendecompra.precio,
                    ordendecompra.numerocheque,
                    (detalleordendecompra.cantidad * detalleordendecompra.precio)::numeric(14,2) as importe
                  FROM 
                    public.detalleordendecompra, 
                    public.ordendecompra, 
                    public.producto, 
                    public.formaspago, 
                    public.mayorista
                  WHERE 
                    ordendecompra.ordenid = detalleordendecompra.ordenid AND
                    producto.productoid = detalleordendecompra.productoid AND
                    formaspago.pagoid = ordendecompra.pagoid AND
                    mayorista.mayoristaid = ordendecompra.mayoristaid AND
                    ordendecompra.ordenid =:p_numero_venta
                  ORDER BY 
                    detalleid;
                  ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_numero_venta", $numeroVenta);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    


}
