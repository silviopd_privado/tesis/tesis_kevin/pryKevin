<?php

require_once '../datos/Conexion.clase.php';

class TipoCategoria extends Conexion {

    private $tipoCategoriaID;
    private $categoriaID;
    private $descripcion;
    private $comentario;

    function getTipoCategoriaID() {
        return $this->tipoCategoriaID;
    }

    function getCategoriaID() {
        return $this->categoriaID;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getComentario() {
        return $this->comentario;
    }

    function setTipoCategoriaID($tipoCategoriaID) {
        $this->tipoCategoriaID = $tipoCategoriaID;
    }

    function setCategoriaID($categoriaID) {
        $this->categoriaID = $categoriaID;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setComentario($comentario) {
        $this->comentario = $comentario;
    }

    
    public function listar( $p_codigoCategoria ) {
        try {
            $sql = "select * from f_listar_tipocategoria(:p_codigoCategoria)";
           
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            
            return $resultado;
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoTipoCategoria) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from tipocategoria where tipocategoriaid = :p_tipocategoriaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_tipocategoriaid", $p_codigoTipoCategoria);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE public.tipocategoria SET categoriaid=:p_categoriaid, descripcion=:p_descripcion, comentario=:p_comentario WHERE tipocategoriaid=:p_tipocategoriaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_categoriaid", $this->getCategoriaID());
            $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
            $sentencia->bindParam(":p_comentario", $this->getComentario());
            $sentencia->bindParam(":p_tipocategoriaid", $this->getTipoCategoriaID());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla tipo categoria.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('tipocategoria') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $tipoCategoriaid = $resultado["nc"];
                $this->setTipoCategoriaID($tipoCategoriaid);
                $sql = "INSERT INTO public.tipocategoria(tipocategoriaid, categoriaid, descripcion, comentario) VALUES (:p_tipocategoriaid, :p_categoriaid, :p_descripcion, :p_comentario);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_tipocategoriaid", $this->getTipoCategoriaID());
                $sentencia->bindParam(":p_categoriaid", $this->getCategoriaID());
                $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
                $sentencia->bindParam(":p_comentario", $this->getComentario());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'tipocategoria';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla tipo categoria.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_tipoCategoriaid) {
        try {
            $sql = "select * from tipocategoria where tipocategoriaid = :p_tipocategoriaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_tipocategoriaid", $p_tipoCategoriaid);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
