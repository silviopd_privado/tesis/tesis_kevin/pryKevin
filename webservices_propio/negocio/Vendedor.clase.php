<?php

require_once '../datos/Conexion.clase.php';

class Vendedor extends Conexion {

    private $vendedorid;
    private $nombre;
    private $apellidopaterno;
    private $apellidomaterno;
    private $direccion;
    private $telefono;
    private $email;
    private $fechanacimiento;
    private $fechaingreso;
    private $fechatermino;

    function getVendedorid() {
        return $this->vendedorid;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellidopaterno() {
        return $this->apellidopaterno;
    }

    function getApellidomaterno() {
        return $this->apellidomaterno;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    function getFechaingreso() {
        return $this->fechaingreso;
    }

    function getFechatermino() {
        return $this->fechatermino;
    }

    function getEstado() {
        return $this->estado;
    }

    function setVendedorid($vendedorid) {
        $this->vendedorid = $vendedorid;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellidopaterno($apellidopaterno) {
        $this->apellidopaterno = $apellidopaterno;
    }

    function setApellidomaterno($apellidomaterno) {
        $this->apellidomaterno = $apellidomaterno;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;
    }

    function setFechaingreso($fechaingreso) {
        $this->fechaingreso = $fechaingreso;
    }

    function setFechatermino($fechatermino) {
        $this->fechatermino = $fechatermino;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    public function listar() {
        try {
            $sql = "SELECT 
                    vendedor.vendedorid, 
                    (vendedor.nombre || ' ' || vendedor.apellidopaterno || ' ' || vendedor.apellidomaterno)::varchar as nombre, 
                    vendedor.direccion, 
                    vendedor.telefono, 
                    vendedor.email, 
                    vendedor.fechanacimiento, 
                    vendedor.fechaingreso, 
                    vendedor.fechatermino
                  FROM 
                    public.vendedor;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_vendedorid) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from vendedor where vendedorid = :p_vendedorid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_vendedorid", $p_vendedorid);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE public.vendedor
                    SET nombre=:p_nombre, apellidopaterno=:p_apellidopaterno, 
                        apellidomaterno=:p_apellidomaterno,direccion=:p_direccion,
                        telefono=:p_telefono, email=:p_email,
                        fechanacimiento=:p_fechanacimiento, fechaingreso=:p_fechaingreso, 
                        fechatermino=:p_fechatermino, estado=:p_estado
                    WHERE vendedorid=:p_vendedorid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_fechanacimiento", $this->getFechanacimiento());
            $sentencia->bindParam(":p_fechaingreso", $this->getFechaingreso());
            $sentencia->bindParam(":p_fechatermino", $this->getFechatermino());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_vendedorid", $this->getVendedorid());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla vendedor.");
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        try {

            $sql = "INSERT INTO public.vendedor(
                    vendedorid, nombre, apellidopaterno, apellidomaterno, direccion, 
                    telefono, email, fechanacimiento, fechaingreso, fechatermino)
                    VALUES (:p_vendedorid, :p_nombre, 
                    :p_apellidopaterno, :p_apellidomaterno,
                    :p_direccion,:p_telefono,
                    :p_email, :p_fechanacimiento,
                    :p_fechaingreso, :p_fechatermino);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_vendedorid", $this->getVendedorid());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_apellidopaterno", $this->getApellidopaterno());
            $sentencia->bindParam(":p_apellidomaterno", $this->getApellidomaterno());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_fechanacimiento", $this->getFechanacimiento());
            $sentencia->bindParam(":p_fechaingreso", $this->getFechaingreso());
            $sentencia->bindParam(":p_fechatermino", $this->getFechatermino());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function leerDatos($p_vendedorid) {
        try {
            $sql = "select * from vendedor where vendedorid = :p_vendedorid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_vendedorid", $p_vendedorid);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function cargarDatosVendedorDepartamentoProvincia($nombre) {
        try {
            $sql = "SELECT * from f_listar_provincia_departamento(:p_nombre)";
            $sentencia = $this->dblink->prepare($sql);
            $nombre = '%' . strtolower($nombre) . '%';
            $sentencia->bindParam(":p_nombre", $nombre);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
