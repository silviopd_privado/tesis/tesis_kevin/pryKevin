<?php

require_once '../datos/Conexion.clase.php';

class Provincia extends Conexion {

    public function listar($p_departamentoid) {
        try {
            $sql = "SELECT 
                    provincia.provinciaid, 
                    provincia.nombreprovincia, 
                    departamento.departamentoid
                  FROM 
                    public.departamento, 
                    public.provincia
                  WHERE 
                    departamento.departamentoid = provincia.departamentoid AND 
                    departamento.departamentoid = :p_departamentoid";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_departamentoid", $p_departamentoid);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
