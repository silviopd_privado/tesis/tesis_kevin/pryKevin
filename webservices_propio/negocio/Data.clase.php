<?php

require_once '../datos/Conexion.clase.php';

class Data extends Conexion {

    private $data; // JSON

    function getData() {
        return $this->data;
    }

    function setData($data) {
        $this->data = $data;
    }

    
    public function agregar() {
        try {
//            $sql = "select * from f_agregar_data(:p_data,:p_usuario)";
            $sql = "select * from f_agregar_data2(:p_data)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_data", $this->getData());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
