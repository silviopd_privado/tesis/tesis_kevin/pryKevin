<?php

require_once '../datos/Conexion.clase.php';

class Categoria extends Conexion {

    private $categoriaID;
    private $descripcion;
    private $observacion;

    function getCategoriaID() {
        return $this->categoriaID;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function setCategoriaID($categoriaID) {
        $this->categoriaID = $categoriaID;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    public function listar() {
        try {
            $sql = "select * from categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoCategoria) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from categoria where categoriaid = :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update categoria set descripcion = :p_descripcion , observacion = :p_observacion where categoriaid = :p_categoriaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
            $sentencia->bindParam(":p_observacion", $this->getObservacion());
            $sentencia->bindParam(":p_categoriaid", $this->getCategoriaID());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla categoria.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('categoria') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $categoriaid = $resultado["nc"];
                $this->setCategoriaID($categoriaid);
                $sql = "INSERT INTO categoria(categoriaid, descripcion,observacion) VALUES (:p_categoriaid, :p_descripcion,:p_observacion);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_categoriaid", $this->getCategoriaID());
                $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
                $sentencia->bindParam(":p_observacion", $this->getObservacion());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'categoria';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla categoria.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_categoriaid) {
        try {
            $sql = "select * from categoria where categoriaid = :p_categoriaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_categoriaid", $p_categoriaid);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
