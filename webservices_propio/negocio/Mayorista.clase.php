<?php

require_once '../datos/Conexion.clase.php';

class Mayorista extends Conexion {

    private $mayoristaid;
    private $razonsocial;
    private $email;
    private $telefono;
    private $webservice;
    private $direccio;
    private $provinciaid;
    private $departamentoid;
    private $estado;
    private $credito;

    function getMayoristaid() {
        return $this->mayoristaid;
    }

    function getRazonsocial() {
        return $this->razonsocial;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getWebservice() {
        return $this->webservice;
    }

    function getDireccio() {
        return $this->direccio;
    }

    function getProvinciaid() {
        return $this->provinciaid;
    }

    function setMayoristaid($mayoristaid) {
        $this->mayoristaid = $mayoristaid;
    }

    function setRazonsocial($razonsocial) {
        $this->razonsocial = $razonsocial;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setWebservice($webservice) {
        $this->webservice = $webservice;
    }

    function setDireccio($direccio) {
        $this->direccio = $direccio;
    }

    function setProvinciaid($provinciaid) {
        $this->provinciaid = $provinciaid;
    }

    function getEstado() {
        return $this->estado;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function getDepartamentoid() {
        return $this->departamentoid;
    }

    function setDepartamentoid($departamentoid) {
        $this->departamentoid = $departamentoid;
    }

    function getCredito() {
        return $this->credito;
    }

    function setCredito($credito) {
        $this->credito = $credito;
    }

    public function listar() {
        try {
            $sql = "select * from f_listar_mayorista()";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_mayoristaid) {
        $this->dblink->beginTransaction();
        try {
            $sql = "update mayorista set estado='I' where mayoristaid = :p_mayoristaid;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $p_mayoristaid);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_editar_mayorista(:p_mayoristaid,:p_razonsocial,:p_email, :p_telefono, :p_webservice, :p_direccion, :p_provinciaid,:p_departamentoid,:p_estado,:p_credito)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $this->getMayoristaid());
            $sentencia->bindParam(":p_razonsocial", $this->getRazonsocial());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_webservice", $this->getWebservice());
            $sentencia->bindParam(":p_direccion", $this->getDireccio());
            $sentencia->bindParam(":p_provinciaid", $this->getProvinciaid());
            $sentencia->bindParam(":p_departamentoid", $this->getDepartamentoid());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_credito", $this->getCredito());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla mayorista.");
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_agregar_mayorista(:p_mayoristaid, :p_razonsocial, :p_email, :p_telefono, :p_webservice, :p_direccion,:p_provinciaid,:p_departamentoid,:p_credito);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $this->getMayoristaid());
            $sentencia->bindParam(":p_razonsocial", $this->getRazonsocial());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_webservice", $this->getWebservice());
            $sentencia->bindParam(":p_direccion", $this->getDireccio());
            $sentencia->bindParam(":p_provinciaid", $this->getProvinciaid());
            $sentencia->bindParam(":p_departamentoid", $this->getDepartamentoid());
            $sentencia->bindParam(":p_credito", $this->getCredito());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function leerDatos($p_mayoristaid) {
        try {
            $sql = "select * from f_leerdatos_mayorista(:p_mayoristaid)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $p_mayoristaid);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function mayoristaWS($p_mayoristaid) {
        try {
            $sql = "select webservice from mayorista where mayoristaid=:p_mayoristaid";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_mayoristaid", $p_mayoristaid);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function cargarDatosMayorista($nombre) {
        try {
            $sql = "select  mayoristaid,
                    razonsocial,
                    direccion,
                    telefono,
                    (select credito.saldoactual from public.credito where credito.mayoristaid = mayorista.mayoristaid order by creditoid desc limit 1) as saldoactual
                    from mayorista where lower(razonsocial) like :p_nombre";
            $sentencia = $this->dblink->prepare($sql);
            $nombre = '%' . strtolower($nombre) . '%';
            $sentencia->bindParam(":p_nombre", $nombre);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
