<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

require_once '../negocio/Data.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    if (!isset($_POST["usuario"])) {
        Funciones::imprimeJSON(500, "Debe especificar un usuario", "");
        exit();
    }

    $usuarioid = $_POST["usuario"];

    $obj = new Data();
    $resultado = $obj->listar();

    $listaData = array();
    for ($i = 0; $i < count($resultado); $i++) {
        $data = array(
            "partnumber" => $resultado[$i]["partnumber"],
            "ruc" => $resultado[$i]["ruc"],
            "razonsocial" => $resultado[$i]["razonsocial"],
            "ciudad" => $resultado[$i]["ciudad"],
            "factura" => $resultado[$i]["factura"],
            "fecha" => $resultado[$i]["fecha"],
            "cantidad" => $resultado[$i]["cantidad"],
            "rucmayorista" => $resultado[$i]["rucmayorista"],
            "stock" => $resultado[$i]["stock"],
            "usuarioid" => $usuarioid
        );
        $listaData[$i] = $data;
    }


    $ruta = ("http://localhost:8080/proyecto_kevin/webservices_propio/webservice/recibir.data.php");

    $ch = curl_init($ruta); //inicio

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); //metodo
    curl_setopt($ch, CURLOPT_POSTFIELDS, array("data" => json_encode($listaData))); //data

    $result = curl_exec($ch); //ejecuta
    curl_close($ch); //cierra

    Funciones::imprimeJSON(200, "Se ha insertado....", count($resultado));
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
