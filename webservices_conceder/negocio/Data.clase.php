<?php

require_once '../datos/Conexion.clase.php';

class Data extends Conexion {
    /* VARIABLES NECESARIOS

      partnumber
      ruc
      razonsocial
      ciudad
      factura
      fecha
      cantidad
      rucmayorista
      stock

     */

    public function listar() {
        try {
            $sql = "select * from datos";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
