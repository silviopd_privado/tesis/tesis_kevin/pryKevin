<?php

require_once 'configuracion.php';
require_once '../util/funciones/Funciones.clase.php';

class Conexion {

    protected $dblink;

    public function __construct() {
        $this->abrirConexion();
        //echo "conexión abierta";
    }

    public function __destruct() {
        $this->dblink = NULL;
        //echo "Conexión cerrada";
    }

    protected function abrirConexion() {
        /* Postgresql */
        $servidor = "pgsql:host=" . SERVIDOR_BD . ";port=" . PUERTO_BD . ";dbname=" . NOMBRE_BD;
        /* Postgresql */

        /* SQL Server */
 //       $servidor = "sqlsrv:Server=" . SERVIDOR_BD . "," . PUERTO_BD . ";Database=" . NOMBRE_BD;
        /* SQL Server */

        
        $usuario = USUARIO_BD;
        $clave = CLAVE_BD;

        try {
            $this->dblink = new PDO($servidor, $usuario, $clave);
            $this->dblink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $exc) {
            Funciones::mensaje($exc->getMessage(), "e");
        }//        
        return $this->dblink;
    }

}
