<?php
require_once '../util/funciones/definiciones.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo C_NOMBRE_SOFTWARE; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <?php
        include 'estilos.vista.php';
        ?>

    </head>
    <body class="skin-blue layout-top-nav">
        <!-- Site wrapper -->
        <div class="wrapper">

            <?php
            include 'cabecera.vista.php';
            ?>

            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 class="text-bold text-black" style="font-size: 20px;">Mantenimiento De Personal</h1>
                </section>

                <section class="content">

                    <!-- INICIO del formulario modal -->
                    <small>
                        <form id="frmgrabar">
                            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="titulomodal">Título de la ventana</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="txttipooperacion" id="txttipooperacion" class="form-control">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <p>Dni<input type="text" name="txtcodigo" id="txtcodigo" class="form-control input-sm text-center text-bold" placeholder="" required=""></p>
                                                </div>
                                            </div>

                                            <p>Apellido Paterno <font color = "red">*</font>
                                            <input type="text" name="txtapellidopaterno" id="txtapellidopaterno" class="form-control input-sm" placeholder="" required=""><p>

                                            <p>Apellido Materno <font color = "red">*</font>
                                                <input type="text" name="txtapellidomaterno" id="txtapellidomaterno" class="form-control input-sm" placeholder="" required=""></p>

                                            <p>Nombre <font color = "red">*</font>
                                                <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" placeholder="" required=""></p>

                                            <p>Dirección <font color = "red">*</font>
                                                <input type="text" name="txtdireccion" id="txtdireccion" class="form-control input-sm" placeholder="" required=""></p>

                                            <p>Telefono <font color = "red">*</font>
                                                <input type="text" name="txttelefono" id="txttelefono" class="form-control input-sm" placeholder="" required=""></p>

                                            <p>Correo <font color = "red">*</font>
                                                <input type="text" name="txtcorreo" id="txtcorreo" class="form-control input-sm" placeholder="" required=""></p>

                                            <p>
                                                Cargo <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbocargomodal" id="cbocargomodal" required="" >
                                                </select>
                                            </p>

                                            <div class="row" id="departamento-provincia">
                                                <div class="col-xs-12">
                                                    <div class="col-xs-10">
                                                        <div class="form-group">
                                                            <label>Digite las iniciales de la provincia que desea asignar</label>
                                                            <input type="text" class="form-control input-sm" id="txtnombreprovincia" />
                                                            <input type="hidden" id="txtcodigoprovincia" />
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-2">
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <br>
                                                            <button type="button" class="btn btn-danger btn-sm" id="btnagregarmodal">Agregar</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <table id="tabla-listado" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center">CÓDIGO</th>
                                                                <th style="text-align: center">DEPARTAMENTO - PROVINCIA</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="detalleventa" >

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <p>
                                                Estado <font color = "red">*</font>
                                                <select class="form-control input-sm" id="cboestado" name="cboestado" required="" >
                                                    <option value="" disabled selected>Seleccione un estado</option>
                                                    <option value="A" >Activo</option>
                                                    <option value="I" >Inactivo</option>
                                                </select>
                                            </p>

                                            <p>
                                                <font color = "red">* Campos obligatorios</font>
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success" aria-hidden="true"><i class="fa fa-save"></i> Grabar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btncerrar"><i class="fa fa-close"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </small>
                    <!-- FIN del formulario modal -->

                    <div class="row">
                        <div class="col-xs-3">
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal" id="btnagregar"><i class="fa fa-copy"></i> Agregar Nuevo Personal</button>
                        </div>
                    </div>
                    <p>
                    <div class="box box-success">
                        <div class="box-body">
                            <div id="listado">
                            </div>
                        </div>
                    </div>
                    </p>
                </section>
            </div>
        </div><!-- ./wrapper -->

        <script src="../util/jquery/jquery.ui.autocomplete.js" type="text/javascript"></script>
        <script src="../util/jquery/jquery.ui.js" type="text/javascript"></script>
        <script src="js/vendedor-departamento-provincia.autocompletar.js" type="text/javascript"></script>

        <script src="js/cargar-combos.js" type="text/javascript"></script>
        <script src="js/personal.js" type="text/javascript"></script>

        <?php
        include 'scripts.vista.php';
        ?>


    </body>
</html>
