//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

$(document).ready(function () {
    listar();
});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtdescripcion").focus();
});

$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#txtobservacion").val("");
    $("#titulomodal").text("Agregar Nueva Categoria.");
});

function listar() {

    var ruta = DIRECCION_WS + "categoria.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>N</th>';
            html += '<th>CODIGO</th>';
            html += '<th>DESCRIPCION</th>';
            html += '<th>OBSERVACION</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td>' + i + '</td>';
                html += '<td align="center">' + item.categoriaID + '</td>';
                html += '<td>' + item.descripcion + '</td>';
                html += '<td>' + item.observacion + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.categoriaID + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.categoriaID + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[2, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

function eliminar(categoriaid) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "/categoria.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, categoriaid: categoriaid}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}


$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "categoria.agregar.php";
                var token = $.cookie('token');
                var descripcion = $("#txtdescripcion").val()
                var observacion = $("#txtobservacion").val()

                $.post(ruta, {token: token, descripcion: descripcion, observacion: observacion}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "categoria.editar.php";
                var token = $.cookie('token');
                var categoriaid = $("#txtcodigo").val()
                var descripcion = $("#txtdescripcion").val()
                var observacion = $("#txtobservacion").val()

                $.post(ruta, {token: token, categoriaid: categoriaid, descripcion: descripcion, observacion: observacion}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(categoriaid) {

    var ruta = DIRECCION_WS + "categoria.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, categoriaid: categoriaid}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#txttipooperacion").val("editar");
                $("#txtcodigo").val(item.categoriaID);
                $("#txtdescripcion").val(item.descripcion);
                $("#txtobservacion").val(item.observacion);
                $("#titulomodal").text("Editar articulo.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}





