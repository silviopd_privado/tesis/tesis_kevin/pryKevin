//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";


$("#btnagregar").click(function () {
    document.location.href = "venta.vista.php";
});


$(document).ready(function () {
    listar();
});

$("#btnfiltrar").click(function () {
    listar();
});


$("input[name='rbtipo']").change(function () {
    if ($("input[name='rbtipo']:checked").val() == 2) {
        $("#txtfecha1").prop("disabled", false);
        $("#txtfecha2").prop("disabled", false);
    } else {
        $("#txtfecha1").prop("disabled", true);
        $("#txtfecha2").prop("disabled", true);
    }
});

function listar() {

    var tipo = $("#rbtipo:checked").val();

    var fechaInicio = $("#txtfecha1").val();

    var fechaFinal = $("#txtfecha2").val();

    var ruta_login = DIRECCION_WS + "venta.listar.php";
    var token = $.cookie('token');

    $.post(ruta_login, {token: token, fechaInicio: fechaInicio, fechaFinal: fechaFinal, tipo: tipo}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado5" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">OPCIONES</th>';

            html += '<th style="font-size:13px">N° ORDEN</th>';
            html += '<th style="font-size:13px">RAZÓN SOCIAL</th>';
            html += '<th style="font-size:13px">SUBTOTAL</th>';
            html += '<th style="font-size:13px">IGV</th>';
            html += '<th style="font-size:13px">TOTAL</th>';
            html += '<th style="font-size:13px">TIPO DE PAGO</th>';
            html += '<th style="font-size:13px">FECHA</th>';
            html += '<th style="font-size:13px">USUARIO</th>';
            html += '<th style="font-size:13px">ESTADO</th>';
            html += '<th style="font-size:13px">NUMERO CHEQUE</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody >';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {

                if (item.tipopago === 'CONTADO' && item.estado === 'PAGADO') {
                    html += '<tr>';
                    html += '<td align="center">';
                    html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-danger btn-xs" onclick="anular(' + item.ordenid + ')" data-toggle="tooltip" data-placement="top" title="Anular"><i class="fa fa-close"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Detalle Venta" onclick="informacion(' + item.ordenid + ')"><i class="fa fa-navicon"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" onclick="cancelar(' + item.ordenid + ')"><i class="glyphicon glyphicon-ok"></i></button>';
                    html += '</td>';

                } else if (item.tipopago === 'CONTADO' && item.estado === 'CANCELADO') {
                    html += '<tr style="text-decoration:line-through; color:red">';
                    html += '<td align="center">';
                    html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-danger btn-xs" onclick="anular(' + item.ordenid + ')"><i class="fa fa-close"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Detalle Venta" onclick="informacion(' + item.ordenid + ')"><i class="fa fa-navicon"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" onclick="cancelar(' + item.ordenid + ')"><i class="glyphicon glyphicon-ok"></i></button>';
                    html += '</td>';
                } else {
                    if (item.estado === 'CANCELADO') {
                        html += '<tr style="text-decoration:line-through; color:red">';
                        html += '<td align="center">';
                        html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-danger btn-xs" onclick="anular(' + item.ordenid + ')"><i class="fa fa-close"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Detalle Venta" onclick="informacion(' + item.ordenid + ')"><i class="fa fa-navicon"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" onclick="cancelar(' + item.ordenid + ')"><i class="glyphicon glyphicon-ok"></i></button>';
                        html += '</td>';
                    } else if (item.estado === 'PAGADO') {
                        html += '<tr>';
                        html += '<td align="center">';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-danger btn-xs" onclick="anular(' + item.ordenid + ')" data-toggle="tooltip" data-placement="top" title="Anular"><i class="fa fa-close"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Detalle Venta" onclick="informacion(' + item.ordenid + ')"><i class="fa fa-navicon"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button disabled="" type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" onclick="cancelar(' + item.ordenid + ')"><i class="glyphicon glyphicon-ok"></i></button>';
                        html += '</td>';
                    } else {
                        html += '<tr>';
                        html += '<td align="center">';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-danger btn-xs" onclick="anular(' + item.ordenid + ')" data-toggle="tooltip" data-placement="top" title="Anular"><i class="fa fa-close"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Detalle Venta" onclick="informacion(' + item.ordenid + ')"><i class="fa fa-navicon"></i></button>';
                        html += '&nbsp;&nbsp;';
                        html += '<button type="button" style="font-size:10px; vertical-align:middle" class="btn btn-primary btn-xs" onclick="cancelar(' + item.ordenid + ')"><i class="glyphicon glyphicon-ok"></i></button>';
                        html += '</td>';
                    }
                }
                html += '<td style="font-size:12px; vertical-align:middle" align="center">' + item.ordenid + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.razonsocial + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.subtotal + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.igv + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.total + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.tipopago + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.fechaemitida + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.usuario + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.estado + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle" >' + item.numerocheque + '</td>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado5').dataTable({
                "aaSorting": [[1, "asc"]],
                "sScrollX": "100%",
                "sScrollXInner": "150%",
                "bScrollCollapse": true,
                "bPaginate": true,
                "bProcessing": true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cancelar(numeroVenta) {

    var ruta_login = DIRECCION_WS + "venta.cancelar.php";
    var token = $.cookie('token');

    swal({
        title: "Confirme",
        text: "¿Esta seguro de pagar la venta seleccionada?",
        type: "input",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Aceptar',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Ingrese el numero de cheque porfavor..."
    },
            function (isConfirm) {

                if (isConfirm) {
                    $.post(
                            ruta_login,
                            {
                                token: token, p_numeroVenta: numeroVenta, p_numerocheque: isConfirm
                            }
                    ).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) { //ok
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        }

                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    });
                }
            });

}

function anular(numeroVenta) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de anular la venta seleccionada?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {
                    var token = $.cookie('token');

                    $.post(
                            DIRECCION_WS + "venta.anular.php",
                            {
                                token: token,
                                p_numeroVenta: numeroVenta
                            }
                    ).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) { //ok
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        }

                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    });

                }
            });

}

function informacion(numeroVenta) {
    var token = $.cookie('token');

    $.post
            (
                    DIRECCION_WS + "venta.listar.detalle.php",
                    {
                        token: token, numeroVenta: numeroVenta
                    }
            ).done(function (resultado) {
        var datosJSON = resultado;

        if (datosJSON.estado === 200) {
            var html = "";
            html += '<small>';
            html += '<table id="tabla-listado" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tbody>';
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td style="font-size:12px; vertical-align:middle" align="center">' + item.detalleid + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle">' + item.codigoproducto + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle; text-align: right">' + item.cantidad + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle; text-align: right">' + item.precio + '</td>';
                html += '<td style="font-size:12px; vertical-align:middle; text-align: right">' + item.importe + '</td>';
                html += '</tr>';

                $("#txtcodigomodal").val(item.ordenid);
                $("#txtfechamodal").val(item.fechaemitida);
                $("#txtfecha1modal").val(item.fechapago);
                $("#txtestadomodal").val(item.estado);
                $("#txttipocompmodal").val(item.tipopago);

                $("#txtporcigvmodal").val(item.porcentaje_igv);
                $("#txtigvmodal").val(item.igv);
                $("#txtsubtotalmodal").val(item.sub_total);
                $("#txttotalmodal").val(item.total);
                $("#txtclientemodal").val(item.razonsocial);
                $("#txtchequemodal").val(item.numerocheque);
            });
            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#detalleventa-informacion").html(html);
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    });
}
