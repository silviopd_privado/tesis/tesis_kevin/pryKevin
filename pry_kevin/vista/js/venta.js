//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";


$("#btnregresar").click(function () {
    document.location.href = "venta.listado.vista.php";
});

$(document).ready(function () {
    obtenerPorcentajeIGV();
});

function obtenerPorcentajeIGV() {

    var ruta_login = DIRECCION_WS + "configuracion.php";
    var token = $.cookie('token');

    $.post(ruta_login, {token: token, p_codigo_parametro: 1}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var valorObtenido = datosJSON.datos;
            $("#txtigv").val(valorObtenido);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}


$("#btnagregar").click(function () {
    var a = 0;

    if ($("#txtcodigoarticulo").val().toString() == '') {
        swal("Verifique", "Debese seleccionar un articulo", "warning");
        $("#txtarticulo").focus();
        return 0;
    }

    var codigoArticulo = $("#txtcodigoarticulo").val();

    $("#detalleventa tr").each(function () {
        var codigotabla = $(this).find("td").eq(0).html();
        if (codigoArticulo === codigotabla) {
            swal("Verifique", "Articulo ya agregado", "warning");

            a = 1;

            $("#txtcodigoarticulo").val("");
            $("#txtarticulo").val("");
            $("#txtarticulo").val("");
            $("#txtprecio").val("");
            $("#txtcantidad").val("");

            $("#txtarticulo").focus();
            return 0;
        }
        ;
    });


    var nombreArticulo = $("#txtarticulo").val();
    var precioVenta = $("#txtprecio").val();
    var cantidad = $("#txtcantidad").val();

    var importe = cantidad * precioVenta;

    //fila para llenar la tabla html en js
    var fila = '<tr>' +
            '<td class="text-center" style="vertical-align:middle;">' + codigoArticulo + '</td>' +
            '<td style="vertical-align:middle;">' + nombreArticulo + '</td>' +
            '<td class="text-right" style="vertical-align:middle;">' + precioVenta + '</td>' +
            '<td class="text-right" style="vertical-align:middle;">' + cantidad + '</td>' +
            '<td class="text-right" style="vertical-align:middle;">' + parseFloat(importe).toFixed(2) + '</td>' +
            '<td id="celiminar" class="text-center" style="font-size:20px" ><a href="javascript:void()"><i class="fa fa-close text-danger"></i></a></td>' +
            '</tr>';

    if (a == 0) {
        //agregar
        $("#detalleventa").append(fila);
        calcularTotales();
    }

    //limpiar
    $("#txtcodigoarticulo").val("");
    $("#txtarticulo").val("");
    $("#txtprecio").val("");
    $("#txtcantidad").val("");

    $("#txtarticulo").focus();
});

//ELIMINAR
$(document).on("click", "#celiminar", function () {
    var filaEliminar = $(this).parents().get(0);

    swal({
        title: "Confirme",
        text: "¿Desea eliminar el registro seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#ff0000',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {

                if (isConfirm) { //el usuario hizo clic en el boton SI     
                    filaEliminar.remove();//<-elimina                    
                    calcularTotales();
                    $("#txtarticulo").focus();
                }
            });
});

function calcularTotales() {
    var total = 0;
    var subTotal = 0;
    var igv = 0;
    var neto = 0;
    var credito = parseFloat($("#txtcredito").val());

    //capturando datos
    $("#detalleventa tr").each(function () {
        var importe = $(this).find("td").eq(4).html();
        subTotal += parseFloat(importe);
    });

    var porcentajeIGV = parseFloat($("#txtigv").val());
    igv = parseFloat(subTotal * (porcentajeIGV / 100));
    total = parseFloat(subTotal + igv);

    neto = parseFloat(credito - total);



    if (neto > 0) {
        $("#txtcredito2").val(neto.toFixed(2));
        neto = 0;
    } else {
        $("#txtcredito2").val("0");
        neto = parseFloat(neto * (-1));
    }

    //asignando
    $("#txtimporteigv").val(igv.toFixed(2));
    $("#txtimportesubtotal").val(subTotal.toFixed(2));
    $("#txttotal").val(total.toFixed(2));
    $("#txtimporteneto").val(neto.toFixed(2));
}

//eliminar backspace(codigo articulo)
$(document).on("keydown", "#txtarticulo", function (e) {
    if (e.keyCode == 8) {
        $("#txtcodigoarticulo").val("");
        $("#txtprecio").val("");
        $("#txtcantidad").val("");
    }
});

//eliminar backspace(codigo cliente)
$(document).on("keydown", "#txtnombrecliente", function (e) {
    if (e.keyCode == 8) {
        $("#txtcodigocliente").val("");
        $("#lbldireccioncliente").val("");
        $("#lbltelefonocliente").val("");

        $("#txtcredito").val("");
        $("#txtcredito2").val("");
    }
});

//validación cantidad
$("#txtcantidad").keypress(function (evento) {
    if (evento.which == 13) {
        evento.preventDefault();
        $("#btnagregar").click();
    } else {
        return validarNumeros(evento);
    }
});


//transaccion agregar
var arrayDetalle = new Array(); //permite almacenar todos los articulos agregados en el detalle de la venta

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar la venta?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    },
            function (isConfirm) {

                if (isConfirm) { //el usuario hizo clic en el boton SI     

                    //procedo a grabar

                    /*CAPTURAR TODOS LOS DATOS NECESARIOS PARA GRABAR EN EL VENTA_DETALLE*/

                    /*limpiar el array*/
                    arrayDetalle.splice(0, arrayDetalle.length);
                    /*limpiar el array*/

                    /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/
                    $("#detalleventa tr").each(function () {
                        var codigoArticulo = $(this).find("td").eq(0).html();
                        var precio = $(this).find("td").eq(2).html();
                        var cantidad = $(this).find("td").eq(3).html();

                        var objDetalle = new Object(); //Crear un objeto para almacenar los datos

                        /*declaramos y asignamos los valores a los atributos*/
                        objDetalle.productoid = codigoArticulo;
                        objDetalle.cantidad = cantidad;
                        objDetalle.precio = precio;
                        /*declaramos y asignamos los valores a los atributos*/

                        arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle

                    });

                    /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/

                    //Convertimos el array "arrayDetalle" a formato de JSON
                    var jsonDetalle = JSON.stringify(arrayDetalle);


                    /*CAPTURAR TODOS LOS DATOS NECESARIOS PARA GRABAR EN EL VENTA_DETALLE*/
                    var ruta_login = DIRECCION_WS + "venta.agregar.php";
                    var token = $.cookie('token');

                    var mayoristaid = $("#txtcodigocliente").val();
                    var pagoid = $("#cbotipocomp").val();
                    var codigo_usuario = $.cookie('codigo_usuario');


                    $.post(ruta_login,
                            {
                                token: token,
                                p_mayoristaid: mayoristaid,
                                p_pagoid: pagoid,
                                p_codigo_usuario: codigo_usuario,
                                p_detalle_venta: jsonDetalle
                            }
                    ).done(function (resultado) {
                        var datosJSON = resultado;

                        if (datosJSON.estado === 200) {
                            swal({
                                title: "Exito",
                                text: datosJSON.mensaje,
                                type: "success",
                                showCancelButton: false,
                                //confirmButtonColor: '#3d9205',
                                confirmButtonText: 'Ok',
                                closeOnConfirm: true,
                            },
                                    function () {
                                        document.location.href = "venta.listado.vista.php";
                                    });
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }

                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    });

                }
            });
});