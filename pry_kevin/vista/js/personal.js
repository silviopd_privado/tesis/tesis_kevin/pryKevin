//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

$(document).ready(function () {
    cargarComboCargo("#cbocargomodal", "seleccione");
    listar();

    $("#departamento-provincia").hide();

    $('.selectpicker').selectpicker({
        style: 'btn-info',
        size: 4
    });

});

$("#btnagregarmodal").click(function () {

    

    var a = 0;

    var codigoArticulo = $("#txtcodigoprovincia").val();

    $("#detalleventa tr").each(function () {
        var codigotabla = $(this).find("td").eq(0).html();
        if (codigoArticulo === codigotabla) {
            swal("Verifique", "Provincia ya agregado", "warning");

            a = 1;

            $("#txtcodigoprovincia").val("");
            $("#txtnombreprovincia").val("");

            $("#txtnombreprovincia").focus();
            return 0;
        }
        ;
    });

    var nombreArticulo = $("#txtnombreprovincia").val();

    //fila para llenar la tabla html en js
    var fila = '<tr>' +
            '<td class="text-center" style="vertical-align:middle;">' + codigoArticulo + '</td>' +
            '<td style="vertical-align:middle;">' + nombreArticulo + '</td>' +
            '<td id="celiminar" class="text-center" style="font-size:20px" ><a href="javascript:void()"><i class="fa fa-close text-danger"></i></a></td>' +
            '</tr>';

    if (a == 0 && codigoArticulo > 0) {
        //agregar
        $("#detalleventa").append(fila);
    }

    //limpiar
    $("#txtcodigoprovincia").val("");
    $("#txtnombreprovincia").val("");

    $("#txtnombreprovincia").focus();
});

$("#txtnombreprovincia").keypress(function (evento) {
    if (evento.which == 13) {
        evento.preventDefault();
        $("#btnagregarmodal").click();
    }
});

//ELIMINAR
$(document).on("click", "#celiminar", function () {
    var filaEliminar = $(this).parents().get(0);

    swal({
        title: "Confirme",
        text: "¿Desea eliminar el registro seleccionado?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#ff0000',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {

                if (isConfirm) { //el usuario hizo clic en el boton SI     
                    filaEliminar.remove();//<-elimina     
                    $("#txtnombreprovincia").focus();
                }
            });
});

$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");
    
    $("#detalleventa").empty();
    
    $("#txtcodigo").val("");
    $("#txtapellidopaterno").val("");
    $("#txtapellidomaterno").val("");
    $("#txtnombre").val("");
    $("#txtdireccion").val("");
    $("#txttelefono").val("");
    $("#txtdireccion").val("");
    $("#txtcorreo").val("");
    $("#cboestado").val("A");
    $("#cbocargomodal").val("");
    $("#departamento-provincia").hide();

    $("#titulomodal").text("Agregar Nuevo Personal.");

    //desactivar el combo de estado
    document.getElementById("cboestado").disabled = true;

    document.getElementById("txtcodigo").disabled = false;

    document.getElementById("cbocargomodal").disabled = false;
});

$("#txtcodigo").focusout(function () {
    var cantidad = $("#txtcodigo").val().length;
    if (cantidad <= 7 || cantidad >= 9) {
        swal("Verifique", "Ingrese dni con 8 números", "warning");
        $("#txtcodigo").focus();
    }
})

$("#cbocargomodal").change(function () {
    if ($("#cbocargomodal").val() == 3) {
        $("#departamento-provincia").show();

        cargarComboDepartamento("#cbodepartamentomodal", "seleccione");
    } else {
        $("#departamento-provincia").hide();
    }
})

function listar() {

    var ruta = DIRECCION_WS + "personal.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado7" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>DNI</th>';
            html += '<th>PERSONAL</th>';
            html += '<th>CARGO</th>';
            html += '<th>DIRECCION</th>';
            html += '<th>TELEFONO</th>';
            html += '<th>CORREO</th>';
            html += '<th>ESTADO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.dni + '</td>';
                html += '<td>' + item.personal + '</td>';
                html += '<td>' + item.cargo + '</td>';
                html += '<td>' + item.direccion + '</td>';
                html += '<td>' + item.telefono + '</td>';
                html += '<td>' + item.correo + '</td>';
                html += '<td>' + item.estado + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.dni + ' , ' + item.cargoid + ')"><i class="fa fa-pencil"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado7').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

var arrayDetalle = new Array();

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var token = $.cookie('token');
                var dni = $("#txtcodigo").val()
                var apellidopaterno = $("#txtapellidopaterno").val()
                var apellidomaterno = $("#txtapellidomaterno").val()
                var nombre = $("#txtnombre").val()
                var cargo = $("#cbocargomodal").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var correo = $("#txtcorreo").val()

                if ($("#cbocargomodal").val() == 3) {

                    arrayDetalle.splice(0, arrayDetalle.length);

                    $("#detalleventa tr").each(function () {
                        var departamentoprovinciaid = $(this).find("td").eq(0).html();

                        var objDetalle = new Object(); //Crear un objeto para almacenar los datos

                        /*declaramos y asignamos los valores a los atributos*/
                        objDetalle.departamentoid = departamentoprovinciaid.substring(0, 2);
                        objDetalle.provinciaid = departamentoprovinciaid.substring(2, 4);
                        /*declaramos y asignamos los valores a los atributos*/

                        arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle

                    });

                    var jsonDetalle = JSON.stringify(arrayDetalle);

                    var ruta = DIRECCION_WS + "personal.vendedor.agregar.php";

                    $.post(ruta, {token: token, dni: dni, apellidopaterno: apellidopaterno, apellidomaterno: apellidomaterno, nombre: nombre, direccion: direccion, telefono: telefono, correo: correo, cargo: cargo, jsonDetalle: jsonDetalle}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            swal("Exito", datosJSON.mensaje, "success");
                            $("#btncerrar").click(); //cerrar ventana
                            listar();//refrescar los datos
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })

                } else {
                    var ruta = DIRECCION_WS + "personal.agregar.php";

                    $.post(ruta, {token: token, dni: dni, apellidopaterno: apellidopaterno, apellidomaterno: apellidomaterno, nombre: nombre, direccion: direccion, telefono: telefono, correo: correo, cargo: cargo}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            swal("Exito", datosJSON.mensaje, "success");
                            $("#btncerrar").click(); //cerrar ventana
                            listar();//refrescar los datos
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            } else {

                var token = $.cookie('token');
                var dni = $("#txtcodigo").val()
                var apellidopaterno = $("#txtapellidopaterno").val()
                var apellidomaterno = $("#txtapellidomaterno").val()
                var nombre = $("#txtnombre").val()
                var cargo = $("#cbocargomodal").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var correo = $("#txtcorreo").val()
                var estado = $("#cboestado").val()

                if ($("#cbocargomodal").val() == 3) {

                    arrayDetalle.splice(0, arrayDetalle.length);

                    $("#detalleventa tr").each(function () {
                        var departamentoprovinciaid = $(this).find("td").eq(0).html();

                        var objDetalle = new Object(); //Crear un objeto para almacenar los datos

                        /*declaramos y asignamos los valores a los atributos*/
                        objDetalle.departamentoid = departamentoprovinciaid.substring(0, 2);
                        objDetalle.provinciaid = departamentoprovinciaid.substring(2, 4);
                        /*declaramos y asignamos los valores a los atributos*/

                        arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle

                    });

                    var jsonDetalle = JSON.stringify(arrayDetalle);

                    var ruta = DIRECCION_WS + "personal.vendedor.editar.php";

                    $.post(ruta, {token: token, dni: dni, apellidopaterno: apellidopaterno, apellidomaterno: apellidomaterno, nombre: nombre, direccion: direccion, telefono: telefono, correo: correo, cargo: cargo, jsonDetalle: jsonDetalle, estado: estado}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            swal("Exito", datosJSON.mensaje, "success");
                            $("#btncerrar").click(); //cerrar ventana
                            listar();//refrescar los datos
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })

                } else {
                    var ruta = DIRECCION_WS + "personal.editar.php";

                    $.post(ruta, {token: token, dni: dni, apellidopaterno: apellidopaterno, apellidomaterno: apellidomaterno, nombre: nombre, cargo: cargo, direccion: direccion, telefono: telefono, correo: correo, estado: estado}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            swal("Exito", datosJSON.mensaje, "success");
                            $("#btncerrar").click(); //cerrar ventana
                            listar();//refrescar los datos
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            }
        }
    });
});

function leerDatos(dni, cargo) {

    document.getElementById("txtcodigo").disabled = true;
    document.getElementById("cboestado").disabled = false;

    var ruta;
    if (cargo == 3) {
        ruta = DIRECCION_WS + "personal.vendedor.leerdatos.php";
    } else {
        ruta = DIRECCION_WS + "personal.leerdatos.php";
    }
    var token = $.cookie('token');

    $("#detalleventa").empty();

    $.post(ruta, {token: token, dni: dni}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                if (item.cargo === 3) {
                    $("#txttipooperacion").val("editar");
                    $("#txtcodigo").val(item.dni)
                    $("#txtapellidopaterno").val(item.apellidopaterno)
                    $("#txtapellidomaterno").val(item.apellidomaterno)
                    $("#txtnombre").val(item.nombre)
                    $("#cbocargomodal").val(item.cargo)
                    $("#txtdireccion").val(item.direccion)
                    $("#txttelefono").val(item.telefono)
                    $("#txtcorreo").val(item.correo)
                    $("#cboestado").val(item.estado)

                    $("#departamento-provincia").show();

                    var fila = '<tr>' +
                            '<td class="text-center" style="vertical-align:middle;">' + item.departamentoprovinciaid + '</td>' +
                            '<td style="vertical-align:middle;">' + item.nombredepartamentoprovincia + '</td>' +
                            '<td id="celiminar" class="text-center" style="font-size:20px" ><a href="javascript:void()"><i class="fa fa-close text-danger"></i></a></td>' +
                            '</tr>';

                    $("#detalleventa").append(fila);

                    document.getElementById("cbocargomodal").disabled = true;

                } else {
                    $("#txttipooperacion").val("editar");
                    $("#txtcodigo").val(item.dni)
                    $("#txtapellidopaterno").val(item.apellidopaterno)
                    $("#txtapellidomaterno").val(item.apellidomaterno)
                    $("#txtnombre").val(item.nombre)
                    $("#cbocargomodal").val(item.cargo)
                    $("#txtdireccion").val(item.direccion)
                    $("#txttelefono").val(item.telefono)
                    $("#txtcorreo").val(item.correo)
                    $("#cboestado").val(item.estado)

                    $("#departamento-provincia").hide();


                    document.getElementById("cbocargomodal").disabled = true;
                }
                $("#titulomodal").text("Editar Personal.");


            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}