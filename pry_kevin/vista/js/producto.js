//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

$(document).ready(function () {
    cargarComboCategoria("#cbocategoria", "todos");
    cargarComboCategoria("#cbocategoriamodal", "seleccione");
    listar();

});

$("#cbocategoria").change(function () {
    var categoriaid = $("#cbocategoria").val();
    cargarComboTipoCategoria("#cbotipocategoria", "todos", categoriaid);

    listar();
});

$("#cbotipocategoria").change(function () {
    listar();
});

$("#cbocategoriamodal").change(function () {
    var categoriaid = $("#cbocategoriamodal").val();
    cargarComboTipoCategoria("#cbotipocategoriamodal", "seleccione", categoriaid);
});

function listar() {

    var codigoTipoCategoria = $("#cbotipocategoria").val();
    if (codigoTipoCategoria === null) {
        codigoTipoCategoria = 0;
    }

    var ruta = DIRECCION_WS + "producto.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, tipocategoriaid: codigoTipoCategoria}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado3" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>CODIGO PRODUCTO</th>';
            html += '<th>MODELO</th>';
            html += '<th>DESCRIPCION</th>';
            html += '<th>CAPACIDAD</th>';
            html += '<th>CATEGORIA</th>';
            html += '<th>TIPO CATEGORIA</th>';
            html += '<th>ESTADO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                if (item.estado === "INACTIVO") {
                    html += '<tr style="text-decoration:line-through; color:red">';
                    html += '<td align="center">' + item.codigoproducto + '</td>';
                    ;
                    html += '<td>' + item.modelo + '</td>';
                    html += '<td>' + item.descripcion + '</td>';
                    html += '<td>' + item.capacidad + '</td>';
                    html += '<td>' + item.categoria + '</td>';
                    html += '<td>' + item.tipocategoria + '</td>';
                    html += '<td>' + item.estado + '</td>';
                    html += '<td align="center">';
                    html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.codigo + ')"><i class="fa fa-pencil"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.codigo + ')"><i class="fa fa-close"></i></button>';
                    html += '</td>';
                    html += '</tr>';
                } else {
                    html += '<tr>';
                    html += '<td align="center">' + item.codigoproducto + '</td>';
                    ;
                    html += '<td>' + item.modelo + '</td>';
                    html += '<td>' + item.descripcion + '</td>';
                    html += '<td>' + item.capacidad + '</td>';
                    html += '<td>' + item.categoria + '</td>';
                    html += '<td>' + item.tipocategoria + '</td>';
                    html += '<td>' + item.estado + '</td>';
                    html += '<td align="center">';
                    html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.codigo + ')"><i class="fa fa-pencil"></i></button>';
                    html += '&nbsp;&nbsp;';
                    html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.codigo + ')"><i class="fa fa-close"></i></button>';
                    html += '</td>';
                    html += '</tr>';
                }

            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado3').dataTable({
                "aaSorting": [[2, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(productoid) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "producto.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, productoid: productoid}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "producto.agregar.php";
                var token = $.cookie('token');
                var codigoproducto = $("#txtcodigoproducto").val()
                var modelo = $("#txtmodelo").val()
                var descripcion = $("#txtdescripcion").val()
                var capacidad = $("#txtcapacidad").val()
                var tipocategoriaid = $("#cbotipocategoriamodal").val();

                $.post(ruta, {token: token, codigoproducto: codigoproducto, modelo: modelo, descripcion: descripcion, capacidad: capacidad, tipocategoriaid: tipocategoriaid}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "producto.editar.php";
                var token = $.cookie('token');
                var productoid = $("#txtcodigo").val()
                var codigoproducto = $("#txtcodigoproducto").val()
                var modelo = $("#txtmodelo").val()
                var descripcion = $("#txtdescripcion").val()
                var capacidad = $("#txtcapacidad").val()
                var estado = $("#cboestado").val();
                var tipocategoriaid = $("#cbotipocategoriamodal").val();

                $.post(ruta, {token: token, productoid: productoid, codigoproducto: codigoproducto, modelo: modelo, descripcion: descripcion, capacidad: capacidad, estado: estado, tipocategoriaid: tipocategoriaid}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtcodigoproducto").val("");
    $("#txtmodelo").val("");
    $("#txtdescripcion").val("");
    $("#txtcapacidad").val("");
    $("#cboestado").val("A");
    $("#cbocategoriamodal").val("");
    $("#cbotipocategoriamodal").empty();
    $("#titulomodal").text("Agregar nuevo Producto");

    //desactivar el combo de estado
    document.getElementById("cboestado").disabled = true;
});

$("#myModal").on("shown.bs.modal", function () {
    $("#txtcodigoproducto").focus();
});

function leerDatos(productoid) {

    //activar el combo de estado
    document.getElementById("cboestado").disabled = false;

    var ruta = DIRECCION_WS + "producto.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, productoid: productoid}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#txttipooperacion").val("editar");
                $("#txtcodigo").val(item.codigo);
                $("#txtcodigoproducto").val(item.codigoproducto);
                $("#txtmodelo").val(item.modelo);
                $("#txtdescripcion").val(item.descripcion);
                $("#txtcapacidad").val(item.capacidad);
                $("#cbocategoriamodal").val(item.categoria);
                //Ejecuta el evento change para llenar las categorías que pertenecen a la categoria seleccionada
                $("#cbocategoriamodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cbotipocategoriamodal").val(item.tipocategoria);
                });
                $("#cboestado").val(item.estado);

                $("#titulomodal").text("Modificar Producto.");

            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

