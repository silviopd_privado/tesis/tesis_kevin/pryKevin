//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

$(document).ready(function () {
    cargarComboCategoria("#cbocategoria", "todos");
    cargarComboCategoria("#cbocategoriamodal", "seleccione");
    listar();
});

$("#cbocategoria").change(function () {
    var codigoCategoria = $("#cbocategoria").val();
    cargarComboCategoria("#cbotipocategoria", codigoCategoria);
    listar();
});

$("#cbocategoria").change(function () {
    var codigoCategoria = $("#cbocategoria").val();
    cargarComboLinea("#cbotipocategoria", codigoCategoria);
});

function listar() {
    var codigoCategoria = $("#cbocategoria").val();
    if (codigoCategoria === null) {
        codigoCategoria = 0;
    }

    var ruta_login = DIRECCION_WS + "tipocategoria.listar.php";
    var token = $.cookie('token');

    $.post(ruta_login, {token: token, categoriaid: codigoCategoria}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado1" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>CODIGO</th>';
            html += '<th>CATEGORIA</th>';
            html += '<th>TIPO CATEGORIA</th>';
            html += '<th>COMENTARIO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.tipoCategoriaID + '</td>';
                html += '<td>' + item.categoria + '</td>';
                html += '<td>' + item.tipocategoria + '</td>';
                html += '<td>' + item.comentario + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.tipoCategoriaID + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.tipoCategoriaID + ')"><i class="fa fa-close"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado1').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}


function eliminar(codigotipocategoria) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta_login = DIRECCION_WS + "tipocategoria.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta_login, {token: token, tipocategoriaid: codigotipocategoria}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}


$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta_login = DIRECCION_WS + "tipocategoria.agregar.php";
                var token = $.cookie('token');
                var descripcion = $("#txtdescripcion").val()
                var comentario = $("#txtcomentario").val()
                var categoriaid = $("#cbocategoriamodal").val();

                $.post(ruta_login, {token: token, categoriaid: categoriaid, descripcion: descripcion, comentario: comentario}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta_login = DIRECCION_WS + "tipocategoria.editar.php";
                var token = $.cookie('token');
                var tipocategoriaid = $("#txtcodigo").val()
                var descripcion = $("#txtdescripcion").val()
                var comentario = $("#txtcomentario").val()
                var categoriaid = $("#cbocategoriamodal").val();

                $.post(ruta_login, {token: token, tipocategoriaid: tipocategoriaid, categoriaid: categoriaid, descripcion: descripcion, comentario: comentario}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});


$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");

    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#txtcomentario").val("");
    $("#cbocategoriamodal").val("");

    $("#titulomodal").text("Agregar Nueva Categoria");

});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtcodigo").focus();
});


function leerDatos(codigotipocategoria) {

    var ruta_login = DIRECCION_WS + "tipocategoria.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta_login, {token: token, tipocategoriaid: codigotipocategoria}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#txttipooperacion").val("editar");
                $("#txtcodigo").val(item.tipoCategoriaID);
                $("#cbocategoriamodal").val(item.categoriaid);
                $("#txtdescripcion").val(item.descripcion);
                $("#txtcomentario").val(item.comentario);

                $("#titulomodal").text("Editar Tipo Categoria.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}




