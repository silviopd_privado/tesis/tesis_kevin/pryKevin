$(document).ready(function () {
    cargarComboMayorista("#cbomayorista", "seleccione");
});

$("#btnagregar").click(function () {

    var ruta = "";

    var token = $.cookie('token');

    var mayorista = $("#cbomayorista").val()

    $.post("http://localhost:8080/proyecto_kevin/webservices_propio/webservice/mayorista.webservice.php", {token: token, rucmayorista: mayorista}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            $.each(datosJSON.datos, function (i, item) {
                ruta = item.mayoristaws;
            });

            $.post(ruta, {usuario: $.cookie('codigo_usuario')}, function () {
            }).done(function (resultado) {
                var datosJSON = resultado;
                if (datosJSON.estado === 200) {
                    swal(datosJSON.mensaje, "Números de registros " + datosJSON.datos, "success");
                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function (error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    });




});