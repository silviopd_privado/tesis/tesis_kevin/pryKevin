//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

$(document).ready(function () {
    cargarComboDepartamento("#cbodepartamentomodal", "seleccione");
    listar();

});

$("#txtmayoristaid").focusout(function () {
    var cantidad = $("#txtmayoristaid").val().length;
    if (cantidad <= 10 || cantidad >= 12) {
        swal("Verifique", "Ingrese ruc con 11 números", "warning");
        $("#txtmayoristaid").focus();
    }
})

$("#cbodepartamentomodal").change(function () {
    var departamentoid = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", departamentoid);
});

function listar() {
    
    var ruta = DIRECCION_WS + "mayorista.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado4" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>RUC</th>';
            html += '<th>RAZON SOCIAL</th>';
            html += '<th>CREDITO</th>';
            html += '<th>EMAIL</th>';
            html += '<th>TELEFONO</th>';
            html += '<th>DIRECCIÓN</th>';
            html += '<th>PROVINCIA</th>';
            html += '<th>DEPARTAMENTO</th>';
            html += '<th>LINK WEBSERVICE</th>';
            html += '<th>ESTADO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.ruc + '</td>';
                html += '<td>' + item.razonsocial + '</td>';
                html += '<td>' + item.saldoactual + '</td>';
                html += '<td>' + item.email + '</td>';
                html += '<td>' + item.telefono + '</td>';
                html += '<td>' + item.direccion + '</td>';
                html += '<td>' + item.provincia + '</td>';
                html += '<td>' + item.departamento + '</td>';
                html += '<td>' + item.webservice + '</td>';
                html += '<td>' + item.estado + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.ruc + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.ruc + ')"><i class="fa fa-close"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado4').dataTable({
                "aaSorting": [[2, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(mayoristaid) {

    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "mayorista.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, mayoristaid: mayoristaid}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "mayorista.agregar.php";
                var token = $.cookie('token');
                var mayoristaid = $("#txtmayoristaid").val()
                var razonsocial = $("#txtrazonsocial").val()
                var email = $("#txtemail").val()
                var telefono = $("#txttelefono").val()
                var webservice = $("#txtwebservice").val()
                var direccion = $("#txtdireccion").val()
                var provinciaid = $("#cboprovinciamodal").val()
                var departamentoid = $("#cbodepartamentomodal").val()
                var credito = $("#txtcredito").val()

                $.post(ruta, {token: token, mayoristaid: mayoristaid, razonsocial: razonsocial, email: email, telefono: telefono, webservice: webservice, direccion: direccion, provinciaid: provinciaid, departamentoid: departamentoid, credito: credito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "mayorista.editar.php";
                var token = $.cookie('token');
                var mayoristaid = $("#txtmayoristaid").val()
                var razonsocial = $("#txtrazonsocial").val()
                var email = $("#txtemail").val()
                var telefono = $("#txttelefono").val()
                var webservice = $("#txtwebservice").val()
                var direccion = $("#txtdireccion").val()
                var provinciaid = $("#cboprovinciamodal").val()
                var departamentoid = $("#cbodepartamentomodal").val()
                var estado = $("#cboestado").val()
                var credito = $("#txtcredito").val()

                $.post(ruta, {token: token, mayoristaid: mayoristaid, razonsocial: razonsocial, email: email, telefono: telefono, webservice: webservice, direccion: direccion, provinciaid: provinciaid, departamentoid: departamentoid, estado: estado, credito: credito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(mayoristaid) {

    document.getElementById("cboestado").disabled = false;
    
    $("#txtmayoristaid").attr('disabled', 'disabled');
    $("#txtmayoristaid").prop('disabled', true);
   
    var ruta = DIRECCION_WS + "mayorista.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, mayoristaid: mayoristaid}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#titulomodal").text("Modificar Mayorista.");
                $("#txttipooperacion").val("editar");

                $("#txtmayoristaid").val(item.ruc);
                $("#txtrazonsocial").val(item.razonsocial);
                $("#txtemail").val(item.email);
                $("#txttelefono").val(item.telefono);
                $("#txtwebservice").val(item.webservice);
                $("#txtdireccion").val(item.direccion);
                $("#cbodepartamentomodal").val(item.departamento);
                //Ejecuta el evento change para llenar las categorías que pertenecen a la categoria seleccionada
                $("#cbodepartamentomodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cboprovinciamodal").val(item.provincia);
                });
                $("#cboestado").val(item.estado);
                $("#txtcredito").val(item.saldoactual);
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");

    $("#txtmayoristaid").val("");
    $("#txtrazonsocial").val("");
    $("#txtemail").val("");
    $("#txttelefono").val("");
    $("#txtwebservice").val("");
    $("#txtdireccion").val("");
    $("#cbodepartamentomodal").val("");
    $("#cboprovinciamodal").empty();
    $("#titulomodal").text("Agregar nuevo mayorista");
    $("#cboestado").val("A");

    //desactivar el combo de estado
    document.getElementById("cboestado").disabled = true;
    
    $("#txtmayoristaid").removeAttr("disabled");
});


