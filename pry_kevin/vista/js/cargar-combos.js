//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";

function cargarComboCategoria(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "categoria.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una categoria</option>';
            } else {
                html += '<option value="0">Todas las categorias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.categoriaID + '">' + item.descripcion + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboTipoCategoria(p_nombreCombo, p_tipo, p_categoriaid) {
    var ruta = DIRECCION_WS + "tipocategoria.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, categoriaid: p_categoriaid}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un tipo de categorias</option>';
            } else {
                html += '<option value="0">Todos los tipos de categorias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.tipoCategoriaID + '">' + item.tipocategoria + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboDepartamento(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "departamento.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una departamento</option>';
            } else {
                html += '<option value="0">Todas los departamentos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.departamentoid + '">' + item.nombredepartamento + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboProvincia(p_nombreCombo, p_tipo, p_departamentoid) {
    var ruta = DIRECCION_WS + "provincia.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, departamentoid: p_departamentoid}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una provincia</option>';
            } else {
                html += '<option value="0">Todas las provincias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.provinciaid + '">' + item.nombreprovincia + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboFormaPago(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "formaspago.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una forma de pago</option>';
            } else {
                html += '<option value="0">Todas las formas de pago</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.pagoid + '">' + item.tipopago + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboMayorista(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "mayorista.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un mayorista</option>';
            } else {
                html += '<option value="0">Todas los mayorista</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.ruc + '">' + item.razonsocial + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboCargo(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "cargo.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un cargo</option>';
            } else {
                html += '<option value="0">Todos los cargos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.cargoid + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}