//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";


/*INICIO: BUSQUEDA DE CLIENTES*/
$("#txtnombreprovincia").autocomplete({
    source: DIRECCION_WS + "vendedor.departamento.provincia.autocompletar.php",
    minLength: 1, //Filtrar desde que colocamos 2 o mas caracteres
    focus: f_enfocar_registro_vendedor_departamento_provincia,
    select: f_seleccionar_registro_vendedor_departamento_provincia
});

function f_enfocar_registro_vendedor_departamento_provincia(event, ui) {
    var registro = ui.item.value;
    $("#txtnombreprovincia").val(registro.nombre);
    event.preventDefault();
}
/*FIN: BUSQUEDA DE CLIENTES*/


/*INICIO: LLENAR CLIENTE CON ENTER*/
function f_seleccionar_registro_vendedor_departamento_provincia(event, ui) {
    var registro = ui.item.value;
    $("#txtnombreprovincia").val(registro.nombre);
    $("#txtcodigoprovincia").val(registro.departamentoprovinciaid);

    event.preventDefault();
}
/*FIN: LLENAR CLIENTE CON ENTER*/
