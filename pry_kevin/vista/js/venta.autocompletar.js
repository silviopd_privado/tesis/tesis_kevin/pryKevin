//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/proyecto_kevin/webservices_propio/webservice/";


/*INICIO: BUSQUEDA DE CLIENTES*/
$("#txtnombrecliente").autocomplete({
    source: DIRECCION_WS + "mayorista.autocompletar.php",
    minLength: 1, //Filtrar desde que colocamos 2 o mas caracteres
    focus: f_enfocar_registro_cliente,
    select: f_seleccionar_registro_cliente
});

function f_enfocar_registro_cliente(event, ui) {
    var registro = ui.item.value;
    $("#txtnombrecliente").val(registro.razonsocial);
    event.preventDefault();
}
/*FIN: BUSQUEDA DE CLIENTES*/


/*INICIO: LLENAR CLIENTE CON ENTER*/
function f_seleccionar_registro_cliente(event, ui) {
    var registro = ui.item.value;
    $("#txtnombrecliente").val(registro.razonsocial);
    $("#txtcodigocliente").val(registro.mayoristaid);
    $("#lbldireccioncliente").val(registro.direccion);
    $("#lbltelefonocliente").val(registro.telefono);
    $("#txtcredito").val(registro.saldoactual);
    $("#txtcredito2").val(registro.saldoactual);

    if (registro.saldoactual > 0) {
        cargarComboFormaPago("#cbotipocomp", "seleccione");
    } else {
        var html = "";
        html += '<option value="2">CONTADO</option>';
        $("#cbotipocomp").html(html);
    }


    $("#txtarticulo").focus();

    event.preventDefault();
}
/*FIN: LLENAR CLIENTE CON ENTER*/



/*INICIO: BUSQUEDA DE ARTICULOS*/
$("#txtarticulo").autocomplete({
    source: DIRECCION_WS + "producto.autocompletar.php",
    minLength: 1, //Filtrar desde que colocamos 2 o mas caracteres
    focus: f_enfocar_registro_articulos,
    select: f_seleccionar_registro_articulos
});

function f_enfocar_registro_articulos(event, ui) {
    var registro = ui.item.value;
    $("#txtarticulo").val(registro.codigoproducto);
    event.preventDefault();
}
/*FIN: BUSQUEDA DE ARTICULOS*/

/*INICIO: LLENAR ARTICULO CON ENTER*/
function f_seleccionar_registro_articulos(event, ui) {
    var registro = ui.item.value;
    $("#txtarticulo").val(registro.nombre);
    $("#txtcodigoarticulo").val(registro.codigo);

    $("#txtprecio").focus();

    event.preventDefault();
}
/*FIN: LLENAR ARTICULO CON ENTER*/